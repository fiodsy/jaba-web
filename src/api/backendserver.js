export const MAIN_SERVICE = 'http://localhost:8080';
export const AUTH_SERVICE = 'http://localhost:9081';

//LOOK-UP
export const ITEM_TYPE = 'itemtype/list';
export const PRODUCT_CATEGORY = 'category/list';

export default MAIN_SERVICE;