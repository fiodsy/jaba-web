import axios from 'axios';
import * as url from '../api/backendserver';
import React from 'react';
import qs from 'qs';


export const register = async ({ email, password }) => {
    try {
        return axios({
            method: 'post',
            url: `${url.MAIN_SERVICE}/v1/account/register`,
            headers: { 'Content-Type': 'application/json' },
            data: { email, password }
        })
    } catch (error) {
        alert(error);
    }
}

export const getUser = async (username, password) => {
    try {
        return axios({
            method: 'post',
            url: `${url.MAIN_SERVICE}/v1/account/login`,
            headers: { 'Content-Type': 'application/json' },
            data: { username, password }
        })
    } catch (error) {
        alert(error);
    }
}

export const login = async (username, password) => {

    const content = {
        username: username,
        password: password,
        grant_type: 'password'
    }

    try {
        return axios({
            method: 'post',
            url: `${url.AUTH_SERVICE}/oauth/token`,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            data: qs.stringify(content),
            auth: {
                username: 'Client',
                password: '{noop}Client_Secret'
            }
        })
    } catch (error) {
        alert(error);
    }
}


export const listAllProvinces = async (token) => {
    try {
        return axios({
            method: 'get',
            url: `${url.MAIN_SERVICE}/v1/address/province`,
            headers: { 'Authorization': 'Bearer ' + token },
        })
    } catch (error) {
        alert(error);
    }
}

export const listAllCityMunicipality = async (token, provinceCode) => {
    try {
        return axios({
            method: 'get',
            url: `${url.MAIN_SERVICE}/v1/address/municipality/${provinceCode}`,
            headers: { 'Authorization': 'Bearer ' + token },
        })
    } catch (error) {
        alert(error);
    }
}

export const listAllBarangays = async (token, municipalCode) => {
    try {
        return axios({
            method: 'get',
            url: `${url.MAIN_SERVICE}/v1/address/barangay/${municipalCode}`,
            headers: { 'Authorization': 'Bearer ' + token },
        })
    } catch (error) {
        alert(error);
    }
}

//ORGANIZATION
export const createOrganization = async (request, accountId, token) => {
    try {
        return axios({
            method: 'post',
            url: `${url.MAIN_SERVICE}/v1/organization/save/${accountId}`,
            headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token },
            data: request
        })
    } catch (error) {
        alert(error);
    }
}

//ITEM
export const createItem = async (request, token) => {
    try {
        return axios({
            method: 'post',
            url: `${url.MAIN_SERVICE}/v1/item/save`,
            headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token },
            data: request
        })
    } catch (error) {
        alert(error);
    }
}

export const fetchLookup = async (token, lookup) => {
    try {
        return axios({
            method: 'get',
            url: `${url.MAIN_SERVICE}/v1/${lookup}`,
            headers: { 'Authorization': 'Bearer ' + token },
        })
    } catch (error) {
        alert(error);
    }
}


//PRODUCT 
export const fetchAllItem = async (token, parameters) => {
    try {
        return axios({
            method: 'post',
            url: `${url.MAIN_SERVICE}/v1/item/find/sort-filter`,
            headers: { 'Authorization': 'Bearer ' + token },
            data: parameters
        })
    } catch (error) {
        alert(error);
    }

}


export const fetchAllProducts = async (token, parameters) => {
    try {
        return axios({
            method: 'post',
            url: `${url.MAIN_SERVICE}/v1/product-bundle/find/sort-filter`,
            headers: { 'Authorization': 'Bearer ' + token },
            data: parameters
        })
    } catch (error) {
        alert(error);
    }

}


export const createProduct = async (request, token) => {
    try {
        return axios({
            method: 'post',
            url: `${url.MAIN_SERVICE}/v1/product-bundle/save`,
            headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token },
            data: request
        })
    } catch (error) {
        alert(error);
    }
}

export const updateProduct = async (request, token) => {
    try {
        return axios({
            method: 'post',
            url: `${url.MAIN_SERVICE}/v1/product-bundle/edit`,
            headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token },
            data: request
        })
    } catch (error) {
        alert(error);
    }
}

export const activateProduct = async (token, productId) => {
    try {
        return axios({
            method: 'patch',
            url: `${url.MAIN_SERVICE}/v1/product-bundle/${productId}/activate`,
            headers: { 'Authorization': 'Bearer ' + token }
        })
    } catch (error) {
        alert(error);
    }
}