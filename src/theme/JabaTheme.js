import { createMuiTheme } from '@material-ui/core/styles';

const JabaTheme = createMuiTheme({
  palette: {
    primary: {
      // light: will be calculated from palette.primary.main,
      main: '#d92424',
    
      // dark: will be calculated from palette.primary.main,
      // contrastText: will be calculated to contrast with palette.primary.main
    },
    secondary: {

      main: '#bdbdbd',
      // dark: will be calculated from palette.secondary.main,
      contrastText: '#000000',
    }, 
    // Used by `getContrastText()` to maximize the contrast between
    // the background and the text.
    contrastThreshold: 3,
    // Used by the functions below to shift a color's luminance by approximately
    // two indexes within its tonal palette.
    // E.g., shift from Red 500 to Red 300 or Red 700.
    tonalOffset: 0.2,
  },
  typography: {
    fontFamily: [
      '"Manrope"'
    ].join(','),
  }, overrides: {
    MuiCssBaseline: {
      "@global": {
        body: {
          backgroundImage: "url('../assets/background/main_bg.jpg')"
        }
      }
    }
  }


});

JabaTheme.typography.h2 = {
  fontWeight: 'bold',
  fontSize: '3.00rem',
  '@media (min-width:600px)': {
    fontSize: '3.25rem',
  },
  '@media (min-width: 1025px) and (max-width: 1280px)': {
    fontSize: '3.00rem',
  },
  [JabaTheme.breakpoints.down('md')]: {
    fontSize: '3.00rem',
  },
  [JabaTheme.breakpoints.up('md')]: {
    fontSize: '3.50rem',
  },
};


JabaTheme.typography.h2 = {
  fontWeight: 400,
  fontSize: '1rem',
  '@media (min-width:600px)': {
    fontSize: '1.25rem',
  },
  [JabaTheme.breakpoints.up('md')]: {
    fontSize: '1.50rem',
  },
};

JabaTheme.typography.h3 = {
  fontWeight: 300,
  fontSize: '0.75rem',
  '@media (min-width:600px)': {
    fontSize: '1.0rem',
  },
  [JabaTheme.breakpoints.up('md')]: {
    fontSize: '1.25rem',
  },
};

JabaTheme.typography.h4 = {
  fontWeight: 200,
  fontSize: '0.75em',
  '@media (min-width:600px)': {
    fontSize: '1.0rem',
  },
  [JabaTheme.breakpoints.up('md')]: {
    fontSize: '1.25rem',
  },
};

JabaTheme.typography.h5 = {
  fontWeight: 100,
  fontSize: '0.50em',
  '@media (min-width:600px)': {
    fontSize: '0.75rem',
  },
  [JabaTheme.breakpoints.up('md')]: {
    fontSize: '1.0rem',
  },
};

export default JabaTheme;