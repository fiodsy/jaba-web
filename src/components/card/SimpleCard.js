import React from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import classes from '../card/SimpleCard.css';
import { withStyles } from '@material-ui/core/styles';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import RatingStar from '../../components/misc/RatingStar/RatingStar';
import Grid from '@material-ui/core/Grid';
import NumberFormat from 'react-number-format';
import Paper from '@material-ui/core/Paper';
import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import Link from '@material-ui/core/Link';
import { NavLink as RouterLink } from 'react-router-dom';
const SupplierName = withStyles({
    root: {
        display: 'block',
        marginBottom: '0px',
        fontSize: '10pt',
        fontWeight: '200',
    },
})(Typography);

const ProductName = withStyles({
    root: {
        display: 'block',
        marginTop: '0px',
        fontSize: '13pt',
        fontWeight: '400'
    },
})(Typography);

const Location = withStyles({
    root: {
        display: 'inline-block',
        marginLeft: '5px',
        fontSize: '10pt',
        fontWeight: '100',


    },
})(Typography);

const LocationIcon = withStyles({
    root: {

        fontSize: '8pt',
        color: '#727272'

    },
})(LocationOnIcon);

const SimpleCard = props => {

    const decorateStatus = () => {
        return (props.isActive === 'true' ? <IconButton onClick={() => props.activateHandler(props.id)} component={'span'} className={classes.editButton} aria-label="activate">
            <CheckCircleIcon fontSize="small" style={{ color: '#4caf50' }} />
        </IconButton> : <IconButton onClick={() => props.activateHandler(props.id)} component={'span'} className={classes.editButton} aria-label="deactivate">
            <CheckCircleIcon fontSize="small" color={'secondary'} />
        </IconButton>)
    }

    return (
        <Card className={classes.cardRoot}>

            <CardMedia
                className={classes.cardMedia}
                image={props.imageUrl}
                title="Contemplative Reptile"
            />
            <Paper>
                <CardContent >
                    <Grid spacing={1} container
                        direction="row"
                        justify="center"
                        alignItems="center">
                        <Grid item xs={9}>
                            <LocationIcon></LocationIcon>
                            <Location variant="h5" component="span">
                                Cavite
                             </Location>
                            <Typography variant="h3" component="div">
                                {props.productName}
                            </Typography>
                            <SupplierName variant="span" component="div">
                                {props.organization}
                            </SupplierName>
                        </Grid>
                        <Grid item xs={3}>
                            <Typography variant="h4" component="span">
                                <NumberFormat value={props.price} displayType={'text'} thousandSeparator={true} prefix="₱" />
                            </Typography>
                        </Grid>
                        <Grid xs={12}>
                            <Typography className={classes.description} variant="body" color="textSecondary" component="p">
                                {props.description}
                            </Typography>
                        </Grid>
                    </Grid>




                    {/* <div>
                        <LocationIcon></LocationIcon>
                        <Location variant="h5" component="span">
                            Cavite
            </Location>
                    </div>
                    <ProductName variant="h5" component="h5">
                        {props.productName}
                    </ProductName>
                    <SupplierName variant="span" component="span">
                        {props.organization}
                    </SupplierName>
                    <Typography variant="body2" color="textSecondary" component="p">
                        {props.description}
                    </Typography> */}
                </CardContent>

                <CardActions >
                    <Grid className={classes.cardAction} container
                        direction="row"
                        justify="center"
                        alignItems="center">
                        <Grid item xs={6}>

                            <RatingStar></RatingStar>
                        </Grid>
                        <Grid className={classes.actionButton} item xs={6}>
                            <Link component={RouterLink} to="/product">
                                <IconButton component={'span'} className={classes.editButton} aria-label="edit">
                                    <EditIcon fontSize="small" color={'secondary'} />
                                </IconButton>
                            </Link>
                            {decorateStatus()}

                            {/* <Button size="small" color="primary">
                                Deactivate
        </Button>
                            <Button size="small" color="primary">
                                Edit
        </Button> */}


                        </Grid>
                    </Grid>
                </CardActions>
            </Paper>
        </Card>
    );
};

SimpleCard.propTypes = {
    imageUrl: PropTypes.string,
    productName: PropTypes.string,
    description: PropTypes.string,
    organization: PropTypes.string
};

export default SimpleCard;