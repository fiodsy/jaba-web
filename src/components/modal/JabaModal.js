import React from 'react'
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import { makeStyles, withStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
    
    },
}));


export const JabaModal = (props) => {
    const classes = useStyles();
    //  const [open, setOpen] = React.useState(props.modalState);
    return (

        <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={props.modalState}
            onClose={props.closeModalHandler}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
                timeout: 500,
            }}
        >
            <Fade in={props.modalState}>
                <div className={classes.paper}>
                    {props.children}
                </div>
            </Fade>
        </Modal>
        
    )
}



export default JabaModal
