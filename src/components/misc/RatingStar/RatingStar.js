import React from 'react';
import PropTypes from 'prop-types';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import GradeIcon from '@material-ui/icons/Grade';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
const RatingStar = props => {


    const RatingList = withStyles({
        root: {
            padding: '0px',
        },
    })(List);

    const RatingListItem = withStyles({
        root: {
            padding: '0px',
        },
    })(ListItem);

    const RatingListItemText = withStyles({
        root: {
            margin: '0px',
        },
    })(ListItemText);

    const ItemIcon = withStyles({
        root: {
            minWidth: '25px'
        }
    })(ListItemIcon);


    const Star = withStyles({
        root: {
            fontSize: '12pt',
            color: '#ffc107',
        },
    })(GradeIcon);

    return (
        <div>
            <RatingList>
                <RatingListItem>
                    <ItemIcon>
                        <Star />
                    </ItemIcon>
                    <RatingListItemText>
                        <Typography variant="body2" color="textSecondary" component="span">
                            5.0 (999+)</Typography>
                    </RatingListItemText>
                </RatingListItem>
            </RatingList>
        </div>
    );
};

RatingStar.propTypes = {

};

export default RatingStar;