import React from 'react';
//import PropTypes from 'prop-types';
import classes from '../stage/MainPageCss.css';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Container from '@material-ui/core/Container';
import LogoBadge from '../../../assets/logo/logo_badge.png';
import SupplierLogo from '../toolbar/logo/SupplierLogo';
import AnnouncementStrip from '../toolbar/announcement/AnnouncementStrip';
import Toolbar from '@material-ui/core/Toolbar';
import { makeStyles, ThemeProvider, withStyles } from '@material-ui/core/styles';
import JabaTheme from '../../../theme/JabaTheme';
import UserAvatar from '../toolbar/avatar/UserAvatar';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import NameAndViewAllPanel from '../../layout/panel/NameAndViewAllPanel/NameAndViewAllPanel';
import SimpleCard from '../../card/SimpleCard';
import sampleProduct1 from '../../../assets/samples/_TP_0967.jpg';
import sampleProduct2 from '../../../assets/samples/Sounds_02.jpg';
import sampleProduct3 from '../../../assets/samples/Cakes_02.jpg';
import List from '@material-ui/core/List';
import RecentReviewListItem from '../../list/RecentReviewListItem/RecentReviewListItem'
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import DashboardComponent from '../../../components/layout/home/DashboardComponent';
import Login from '../../../containers/Login';
import { NavLink as RouterLink } from 'react-router-dom';
import Link from '@material-ui/core/Link';
import { AuthContext } from "../../context/auth";
import PrivateRoute from "../../context/PrivateRoute";
import RegistrationStage from "./RegistrationStage";
import OrganizationStage from "./OrganizationStage";
import ProductStage from "./ProductStage";
import ItemStage from './ItemStage';
import ProductListStage from './ProductListStage';
const JabaMenu = withStyles({
    root: {
        '& #navHome': {
            color: '#3f3e3e'
        },
        '&.MuiToolbar-dense': {
            marginLeft: '110px',
            '& .MuiTypography-body2': {
                margin: '0px 12px',
                color: '#464646',

            },
            '& .MuiLink-underlineHover': {
                color: '#3f3e3e',
                fontSize: '13pt',
                fontWeight: '300',
                fontFamily: 'Manrope',
                '&:hover': {
                    textDecoration: 'none',
                    borderBottom: '2px solid #d92424'
                },
                '&.active': {
                    color: '#000'
                }


            }
        }
    },
})(Toolbar);

const MainPageStage = (props) => {

    const [withBanner, setWithbanner] = React.useState(false);

    const sections = [
        { title: 'Home', url: '/dashboard', withBanner: true },
        { title: 'Organization', url: '/organization', withBanner: true },
        { title: 'Product', url: '/product' },
        { title: 'Item', url: '/item' },
        { title: 'Negotiation', url: '/product-list' },
        { title: 'Booking', url: '/booking' },
        { title: 'Transaction', url: 'transaction' }
    ];




    const { title } = props;
    return (
        <BrowserRouter>
            <React.Fragment>
                <ThemeProvider theme={JabaTheme}>
                    <CssBaseline />

                    <Grid container>


                        <AppBar>
                            <Grid className={classes.header} xs={12}>
                                <Grid className={classes.headerPanel} container>
                                    <Grid xs={12}>
                                        <AnnouncementStrip></AnnouncementStrip>
                                    </Grid>
                                    <Grid xs={12}>
                                        <Container maxWidth="lg">
                                            <Grid
                                                container
                                                direction="row"
                                                justify="flex-end"
                                                alignItems="center"
                                                className={classes.menuPanel}>

                                                <Grid item xs={2}>
                                                    <SupplierLogo></SupplierLogo>
                                                </Grid>
                                                <Grid item xs={8}>
                                                    <JabaMenu component="nav" variant="dense" >
                                                        {sections.map((section) => (
                                                            <Link component={RouterLink} color="inherit"
                                                                noWrap
                                                                key={section.title}
                                                                variant="body2"
                                                                to={section.url}
                                                                id={'nav' + section.title}>
                                                                {section.title}
                                                                onClick={() => {
                                                                    section.withBanner === undefined || section.withBanner ? setWithbanner(false) :
                                                                        setWithbanner(section.withBanner)
                                                                }}
                                                            </Link>
                                                        ))}
                                                    </JabaMenu>
                                                </Grid>
                                                <Grid item xs={2}>
                                                    <UserAvatar></UserAvatar>
                                                </Grid>

                                            </Grid>
                                        </Container>
                                    </Grid>


                                </Grid>


                            </Grid>

                        </AppBar>
                        <Grid container>
                            { withBanner===true ? 
                                <Grid className={classes.bannerPanel} xs={12}>
                                    <Container maxWidth="lg">
                                        <Grid container>
                                            <Grid xs={12}>
                                                <Typography>TEST</Typography>
                                            </Grid>
                                        </Grid>
                                    </Container>
                                </Grid> : null
                            }
                            <PrivateRoute path="/dashboard" exact component={DashboardComponent} />
                            <PrivateRoute path="/organization" exact component={OrganizationStage} />
                            <PrivateRoute path="/product" exact component={ProductStage} />
                            <PrivateRoute path="/item" exact component={ItemStage} />
                            <PrivateRoute path="/product-list" exact component={ProductListStage} />
                        </Grid>
                    </Grid>
                </ThemeProvider>
            </React.Fragment>
        </BrowserRouter>
    );
};

// MainPageStage.propTypes = {

// };

export default MainPageStage;