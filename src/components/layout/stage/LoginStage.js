import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import LoginComponent from '../login/LoginComponent';
import classes from '../stage/LoginStage.css';
import { ThemeProvider } from '@material-ui/core/styles';
import JabaTheme from '../../../theme/JabaTheme';
import { useAuth } from "../../context/auth";
import * as ApiService from '../../../api/ApiService';
import { Link, Redirect } from "react-router-dom";
const LoginStage = (props) => {

    const [username, setUsername] = useState(null);
    const [password, setPassword] = useState(null);
    const {setAuthTokens, setUserId} = useAuth();
    const [isLoggedIn, setLoggedIn] = useState(false);
  


    const submitLogin = (event) => {
        event.preventDefault();
        ApiService.getUser(username, password).then(async response => {
            if (response.status === 200) {
                alert('IN');
                setUserId(response.data.data);
                ApiService.login(username, password).then(async response => {
                    setAuthTokens(response.data.access_token);
                    setLoggedIn(true);
                    alert('IN2');
                });
            }
        });

    }

    const usernameChangeHandler = (event) => {
        setUsername(event.target.value);
    }

    const passwordChangeHandler = (event) => {
        setPassword(event.target.value);
    }

    if (isLoggedIn) {
        return <Redirect to="/dashboard" />;
    }

    return (

        <LoginComponent usernameChange={usernameChangeHandler} passwordChange={passwordChangeHandler}
            submit={submitLogin}></LoginComponent>

    );
};

LoginStage.propTypes = {

};

export default LoginStage; 