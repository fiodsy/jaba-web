import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import JabaTheme from '../../../theme/JabaTheme';
import { makeStyles, ThemeProvider, withStyles } from '@material-ui/core/styles';
import globalclass from '../../../index.css';
import ItemCreateForm from '../item/ItemCreateForm';


const ItemStage = props => {
    return (
        <ThemeProvider theme={JabaTheme}>

            <Grid item xs={8}>
                <Container maxWidth="lg">
                    <Grid container>
                        <Grid xs={12}>
                            <main className={globalclass.content}>
                                <ItemCreateForm></ItemCreateForm>
                            </main>
                        </Grid>
                    </Grid>
                </Container>
            </Grid>

        </ThemeProvider>
    );
};

ItemStage.propTypes = {

};

export default ItemStage;