import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import JabaTheme from '../../../theme/JabaTheme';
import { makeStyles, ThemeProvider, withStyles } from '@material-ui/core/styles';
import globalclass from '../../../index.css';
import ProductCreateForm from '../product/ProductCreateForm';


const ProductStage = props => {
    return (
        <ThemeProvider theme={JabaTheme}>

            <Grid item xs={12}>
                <Container maxWidth="lg">
                    <Grid container>
                        <Grid xs={12}>
                            <main className={globalclass.content}>
                                <ProductCreateForm type="create"></ProductCreateForm>
                            </main>
                        </Grid>
                    </Grid>
                </Container>
            </Grid>
            
        </ThemeProvider>
    );
};

ProductStage.propTypes = {

};

export default ProductStage;