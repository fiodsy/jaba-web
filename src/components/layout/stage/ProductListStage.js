import React, { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import JabaTheme from '../../../theme/JabaTheme';
import { makeStyles, ThemeProvider, withStyles } from '@material-ui/core/styles';
import globalclass from '../../../index.css';
import ProductForm from '../product/ProductForm';
import * as Actions from '../../../redux/action';
import { useDispatch, useSelector } from 'react-redux';
import { fetchAllProduct } from '../../../api/ApiService';
import { useAuth } from '../../context/auth';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import classes from '../../fields/TextField.css';
import TablePagination from '@material-ui/core/TablePagination';
import TableFooter from '@material-ui/core/TableFooter';
import PlainPanel from '../../layout/panel/PlainPanel/PlainPanel';
import PlainSection from '../../layout/panel/PanelSection/PlainSection';
import NumberFormat from 'react-number-format';
import Chip from '@material-ui/core/Chip';
import SimpleCard from '../../card/SimpleCard';
import Checkbox from '@material-ui/core/Checkbox';
import * as url from '../../../api/backendserver';
import Button from '@material-ui/core/Button';
import EditIcon from '@material-ui/icons/Edit';

const ProductListStage = props => {
    const base64Snippet = 'data:image/png;base64,';
    const { authTokens, userId } = useAuth();
    const dispatch = useDispatch();
    const { products, productFilter, productCount, productRow, productPage } = useSelector(state => ({
        productFilter: state.productReducer.filter,
        products: state.productReducer.productData.products,
        productCount: state.productReducer.productData.count,
        productRow: state.productReducer.filter.pagination.size,
        productPage: state.productReducer.filter.pagination.page,
    }));
    const fetchProducts = () => dispatch(Actions.fetchAllProducts(authTokens, productFilter));
    const setProductSize = (size) => dispatch(Actions.setProductSize(size));
    const setProductPage = (page) => dispatch(Actions.setProductPage(page));
    const activateProduct = (productId) => dispatch(Actions.activateProduct(authTokens, productId));
    const selectProduct = (product) => dispatch(Actions.selectProduct(product));

    useEffect(() => {
        (async () => {
            fetchProducts();
        })();


    }, [productFilter]);

    const decorateStatus = (status) => {
        return (status === 'true' ? <Chip size="small" label="Active" style={{ backgroundColor: '#4caf50', color: '#fff' }} /> : <Chip size="small" label="Inactive" color="primary" />)
    }

    const [page, setPage] = React.useState(0);
    const [selectedProduct, setSelectedProduct] = React.useState();
    const handleChangeRowsPerPage = (event) => {
        setProductPage(0);
        setPage(0);
        setProductSize(event.target.value);
    };

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
        setProductPage(newPage * productRow);

    };

    const activateHandler = (productId) => {
        activateProduct(productId);
    }

    const handleClickRow = (id, index) => {
        selectProduct(products[index]);
        setSelectedProduct(
            <Grid container
                direction="row"
                justify="flex-end"
                alignItems="stretch" spacing={1}>
                <Grid item xs={12}>
                    <SimpleCard className={classes.simpleCardProduct} id={products[index].id} description={products[index].description}
                        activateHandler={activateHandler} isActive={products[index].isActive} price={products[index].price} productName={products[index].name} organization={products[index].organization.name} imageUrl={`${url.MAIN_SERVICE}/image/bucket-image/${products[index].images[0].imageId}`}></SimpleCard>
                </Grid>
                <Grid item xs={12}>

                    {/* <Button className={classes.editButton} variant="contained" size="small" color={'primary'}>
                        Edit
</Button> */}
                </Grid>
                <Grid item xs={12}>
                    <Typography component="h5" variant="h5">Note</Typography>
                    <Typography component="p" variant="p" className={classes.productItemTotalDisclaimer}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</Typography>
                </Grid>

            </Grid>);
          

    }

    const renderTable = () => {
        return (
            <TableContainer className={classes.productTableContainer} component={Paper}>
                <Table stickyHeader aria-label="sticky table">
                    <TableHead>
                        <TableRow >

                            <TableCell align="left">Name</TableCell>
                            <TableCell align="center">Description</TableCell>
                            <TableCell align="center">Price</TableCell>
                            <TableCell align="center">Status</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>

                        {products ? products.map((item, index) => (



                            <TableRow hover onClick={() => handleClickRow(item.id, index)} key={item.id}>
                                <TableCell component="th" scope="row">
                                    {item.name}
                                </TableCell>
                                <TableCell align="left">{item.description}</TableCell>
                                <TableCell align="center"><NumberFormat value={item.price} displayType={'text'} thousandSeparator={true} prefix="₱ " /></TableCell>
                                <TableCell align="center">{decorateStatus(item.isActive)}</TableCell>
                            </TableRow>


                        )) : <span>null</span>}
                    </TableBody>
                </Table>

                <TableFooter>
                    <TableRow>
                        <TablePagination
                            rowsPerPageOptions={[5, 10, 15, { value: productCount, label: 'All' }]}
                            component="div"
                            count={productCount}
                            rowsPerPage={productRow}
                            page={page}
                            onChangePage={handleChangePage}
                            onChangeRowsPerPage={handleChangeRowsPerPage}
                        />
                    </TableRow>
                </TableFooter>

            </TableContainer >);
    }
    return (
        <ThemeProvider theme={JabaTheme}>

            <Grid item xs={12}>
                <Container maxWidth="lg">
                    <Grid container>
                        <Grid xs={12}>
                            <main className={globalclass.content}>

                                <PlainPanel panelName='Products'>
                                    <Grid container spacing={2}>
                                        <Grid item xs={12}>

                                            <PlainSection panelName='Product Details'>
                                                <Grid container spacing={2}>
                                                    <Grid item xs={8}>
                                                        {renderTable()}
                                                    </Grid>
                                                    <Grid item xs={4}>
                                                        {selectedProduct}
                                                    </Grid>
                                                </Grid>
                                            </PlainSection>
                                        </Grid>
                                    </Grid>
                                </PlainPanel>
                            </main>
                        </Grid>
                    </Grid>
                </Container>
            </Grid>

        </ThemeProvider>
    );
};

ProductListStage.propTypes = {

};

export default ProductListStage;