import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import JabaTheme from '../../../theme/JabaTheme';
import { makeStyles, ThemeProvider, withStyles } from '@material-ui/core/styles';
import classes from '../stage/OrganizationStage.css';
import globalclass from '../../../index.css';
import OrganizationCreateForm from '../organization/OrganizationCreateForm';
const OrganizationStage = (props) => {
    return (
        <ThemeProvider theme={JabaTheme}>

            <Grid item xs={8}>
                <Container maxWidth="lg">
                    <Grid container>
                        <Grid xs={12}>
                            <main className={globalclass.content}>
                                <OrganizationCreateForm></OrganizationCreateForm>
                            </main>
                        </Grid>
                    </Grid>
                </Container>
            </Grid>
            <Grid item xs={4}>
                <Container maxWidth="lg">
                    <Grid container>
                        <Grid xs={12}>
                            <main className={globalclass.content}>
                                <OrganizationCreateForm></OrganizationCreateForm>
                            </main>
                        </Grid>
                    </Grid>
                </Container>
            </Grid>

        </ThemeProvider>
    );
};

OrganizationStage.propTypes = {

};

export default OrganizationStage;