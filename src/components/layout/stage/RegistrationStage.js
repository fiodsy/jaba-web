import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import classes from '../stage/RegistrationStage.css';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import HeaderLogoBadge from '../../layout/toolbar/logo/HeaderLogoBadge'
import { Typography, withStyles } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/core/styles';
import JabaTheme from '../../../theme/JabaTheme';
import Hidden from '@material-ui/core/Hidden';
import RegistrationComponent from '../../layout/login/RegistrationComponent';
import { BrowserRouter, Route } from 'react-router-dom';
import * as ApiService from '../../../api/ApiService';
import { Link, Redirect } from "react-router-dom";
const RegistrationStage = (props) => {

    const [email, setEmail] = useState(null);
    const [password, setPassword] = useState(null);
    const [confirmed, setConfirmed] = useState(false);
    const submitRegistration = (event) => {
        event.preventDefault();
        const request = {
            email: email,
            password: password
        };
        ApiService.register(request).then(async response => {
            if (response.status === 200) {
                alert("INNNN");
                setConfirmed(true);
            }
        });
    }

    const emailChangeHandler = (event) => {
        setEmail(event.target.value);

    }

    const passwordChangeHandler = (event) => {
        setPassword(event.target.value);
    }

    const BeSupplier = withStyles({
        root: {
            color: '#d92424',
            fontWeight: 'bold',
            marginBottom: '15px'
        },
    })(Typography);


    const Description = withStyles({
        root: {
            paddingRight: '100px',
            fontSize: '15pt',
            color: '#212121',
            marginBottom: '20px',
        },
    })(Typography);

    if (confirmed) {
        return <Redirect to="/login" />;
    }


    return (
        <BrowserRouter>
            <Grid container>
                <ThemeProvider theme={JabaTheme}>

                    <Grid className={classes.firstSection} md={2}>

                    </Grid>
                    <Grid className={classes.firstSection} md={4}>

                        <HeaderLogoBadge className={classes.logo}></HeaderLogoBadge>


                        <Grid className={classes.typoSection}
                            container
                            direction="row"
                            justify="center"
                            alignItems="stretch"
                        >
                            <Grid lg={12}>
                                <BeSupplier variant="h2" component="h2">BE A SUPPLIER?</BeSupplier>
                                <Description variant="p" component="p">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</Description>
                                <Description variant="p" component="p"> Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. </Description>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid md={4}>
                        <RegistrationComponent emailChange={emailChangeHandler} passwordChange={passwordChangeHandler}
                            submit={submitRegistration} className={classes.formContainer}></RegistrationComponent>
                    </Grid>
                    <Grid md={2}>

                    </Grid>
                </ThemeProvider>
            </Grid>
        </BrowserRouter>
    );
};

// RegistrationStage.propTypes = {

// };

export default RegistrationStage;