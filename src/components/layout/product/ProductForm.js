import React, { useEffect } from 'react';
import PlainPanel from '../panel/PlainPanel/PlainPanel';
import PlainSection from '../panel/PanelSection/PlainSection';
import Grid from '@material-ui/core/Grid';
import JabaTextField from '../../fields/JabaTextField/JabaTextField';
import { useDispatch, useSelector } from 'react-redux';
import TextArea from '../../fields/TextArea/TextArea';
import * as Actions from '../../../redux/action';
import ImageUploadAdvance from '../../fields/ImageUploadAdvance/ImageUploadAdvance';
import ItemTable from '../item/ItemTable';
import * as ApiService from '../../../api/ApiService';
import { useAuth } from '../../context/auth';
import Button from '@material-ui/core/Button';
import SelectField from '../../fields/Select/SelectField';
import * as url from '../../../api/backendserver';
import ChipArrayField from '../../fields/ChipArrayField/ChipArrayField';
const ProductForm = props => {

    const { productImages, product, productCategories, isUpdate } = useSelector(state => ({
        productImages: state.productReducer.data.base64Images,
        product: state.productReducer.data,
        productCategories: state.lookUpReducer.productCategories,
        isUpdate: state.productReducer.data.edit,
    }));

    const dispatch = useDispatch();
    const updateFieldValue = (name, value) => dispatch(Actions.productFieldChangeHandler(name, value));
    const addItemHandler = (item) => dispatch(Actions.addProductItem(item));
    const removeItemHandler = (index) => dispatch(Actions.removeProductItem(index));
    const addHashtag = (value) => dispatch(Actions.addProductHashTag(value));
    const removeHashtag = (index) => dispatch(Actions.removeProductHashTag(index));
    //  const initializeProduct = () => dispatch(Actions.initializeProduct());
    const fieldChangeHandler = (event) => {
        const value = event.target.value;
        updateFieldValue([event.target.name], value);
    }

    const priceHandler = (value) => {
        updateFieldValue("price", value);
    }

    const [edit, setEdit] = React.useState(props.type === "edit" ? true : false);
    const editHandler = (isSave) => {
        if (isSave) {
            alert("save");
        }
    }

    const { authTokens, userId } = useAuth();
    const onClickSubmit = (event) => {
        event.preventDefault();
        updateFieldValue("base64CoverImage", productImages[0]);
        ApiService.createProduct(product, authTokens).then(async response => {
            if (response.status === 200) {

            }
        });

    };

    const onClickUpdate = (event) => {
        event.preventDefault();
        // updateFieldValue("base64CoverImage", productImages[0]);
        ApiService.updateProduct(product, authTokens).then(async response => {
            if (response.status === 200) {

            }
        });

    };

    const addProductImage = (image) => {
        dispatch(Actions.addProductImage(image));
    }

    const removeProductImage = (index) => {
        dispatch(Actions.removeProductImage(index));
    }

    useEffect(() => {
        (async () => {
            dispatch(Actions.fetchProductCategoryLookUp(authTokens, url.PRODUCT_CATEGORY));
            if (props.type !== "edit") {
                // initializeProduct();
            }
        })();
    }, []);

    return (
        <div>
            <PlainPanel panelName='Product Details'>
                <Grid container spacing={2}>
                    <Grid item xs={12}>

                        <Grid container spacing={2}>
                            <Grid item xs={6}>
                                <JabaTextField updateHandler={onClickUpdate} editHandler={editHandler} edit value={product.name} fieldStyle="hor" fieldSize="35" onChange={fieldChangeHandler} fieldName="Product Name" name="name" description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
XXX px x XXX px"></JabaTextField>
                                <TextArea value={product.description} fieldStyle="hor" fieldSize="350" onChange={fieldChangeHandler} fieldName="Product Description" name="description" description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
XXX px x XXX px"></TextArea>
                                <SelectField value={product.categoryCode} onChange={fieldChangeHandler} fieldStyle="hor" choices={productCategories} fieldName="Category" name="categoryCode" description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
XXX px x XXX px"></SelectField>
                                <ChipArrayField addHandler={addHashtag} fieldStyle="hor" dataHandler={product.hashtags} removeHandler={removeHashtag} fieldName="Hashtags" name="hashtags" description="Lorem ipsum dolor sit amet, consectetur adipiscing elit." ></ChipArrayField>


                            </Grid>
                            <Grid item xs={6}>
                                <ImageUploadAdvance imageList={productImages} removeImageHandler={removeProductImage} addImageHandler={addProductImage} fieldStyle="hor" enableFieldWrapper={false} fieldName="Images" description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
XXX px x XXX px"></ImageUploadAdvance>

                            </Grid>
                        </Grid>

                    </Grid>

                    <PlainSection>
                        <ItemTable priceHandler={priceHandler} itemAddHandler={addItemHandler} itemRemoveHandler={removeItemHandler} description="Lorem ipsum dolor sit amet, consectetur adipiscing elit." fieldName="Item List"></ItemTable>
                    </PlainSection>
                    <form noValidate >
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            onClick={onClickSubmit}
                        >
                            Submit
              </Button>

                    </form>
                </Grid>
            </PlainPanel>

        </div>
    );
};

ProductForm.propTypes = {

};

export default ProductForm;