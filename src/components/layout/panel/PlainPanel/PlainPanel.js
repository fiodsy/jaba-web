import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import classes from '../PlainPanel/PlainPanel.css';
import { withStyles } from '@material-ui/core/styles';
const PanelContent = withStyles({
    root: {
        marginTop: '15px'
    },
})(Grid);
const PlainPanel = props => {


    return (
        <Grid className={classes.panel} container>
            <Grid item xs={12}>
                <Typography gutterBottom variant="h3" component="h3">{props.panelName}</Typography>
            </Grid>
            <PanelContent item xs={12}>
                {props.children}
            </PanelContent>
        </Grid>
    );
};

PlainPanel.propTypes = {
    panelName: PropTypes.string,
};

export default PlainPanel;