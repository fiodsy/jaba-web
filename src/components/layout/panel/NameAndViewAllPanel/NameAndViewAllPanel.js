import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import classes from '../NameAndViewAllPanel/NameAndViewAllPanel.css';
import { withStyles } from '@material-ui/core/styles';
const PanelLink = withStyles({
    root: {
        fontSize: '12pt',
        fontFamily: 'Manrope',
        fontWeight: 200,
        lineHeight: 1.5
    },
})(Link);

const PanelContent = withStyles({
    root: {
        marginTop: '15px'
    },
})(Grid);
const NameAndViewAllPanel = props => {


    return (
        <Grid className={classes.panel} container>

            <Grid item xs={6}>
                <Typography gutterBottom variant="h3" component="h3">{props.panelName}</Typography>
            </Grid>
            <Grid item xs={6}>
                <PanelLink className={classes.panelLink} color="inherit"
                    noWrap

                    variant="body2"
                    href="http://test.com">
                    View all
                </PanelLink>
            </Grid>

            <PanelContent xs={12}>
                {props.children}
            </PanelContent>
        </Grid>
    );
};

NameAndViewAllPanel.propTypes = {
    viewLink: PropTypes.string,
    panelName: PropTypes.string,
};

export default NameAndViewAllPanel;