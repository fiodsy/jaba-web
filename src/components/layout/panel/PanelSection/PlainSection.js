import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import classes from '../PanelSection/PlainSection.css';
import { withStyles } from '@material-ui/core/styles';

const PlainSection = props => {


    return (
        <Grid className={classes.panel} container>
            <Grid item xs={12}>
                <Typography gutterBottom variant="h3" component="h3">{props.panelName}</Typography>
            </Grid>
            <Grid item xs={12}>
                {props.children}
            </Grid>
        </Grid>
    );
};

PlainSection.propTypes = {
    panelName: PropTypes.string,
};

export default PlainSection;