import React, { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../../redux/action';
import { useAuth } from '../../context/auth';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';
import TablePagination from '@material-ui/core/TablePagination';
import classes from '../../fields/TextField.css';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import SearchIcon from '@material-ui/icons/Search';
import TableFooter from '@material-ui/core/TableFooter';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import Modal from '@material-ui/core/Modal';
import ItemImageGallery from './ItemImageGallery';
import InfoIcon from '@material-ui/icons/Info';
import NumberFormat from 'react-number-format';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import RemoveIcon from '@material-ui/icons/Remove';
import JabaModal from '../../modal/JabaModal';
import ItemDetailedView from './ItemDetailedView';
import * as url from '../../../api/backendserver';
const StyledTableRow = withStyles((theme) => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
    },
}))(TableRow);

const AdditionalChargeRow = withStyles((theme) => ({
    root: {
        '& td': { borderBottom: 'none' }
    },
}))(TableRow);

const TotalRow = withStyles((theme) => ({
    root: {

        backgroundColor: 'green',
        '& td': {
            color: 'white',
            fontWeight: 'bolder',
            fontSize: '15px'
        }

    },
}))(TableRow);

const FieldName = withStyles({
    root: {

        paddingBottom: '0px !important',
        paddingTop: '0px !important',
    },
})(Grid);


export const ItemTable = (props) => {
    const dispatch = useDispatch();
    const fetchAllItem = (token, parameters) => dispatch(Actions.fetchAllItem(token, parameters));
    const addItemFilter = (key, value) => dispatch(Actions.addItemParameterMatches(key, value));
    const addItemFilterLike = (key, value) => dispatch(Actions.addItemParameterLike(key, value));
    const setItemSize = (size) => dispatch(Actions.setItemSize(size));
    const setItemPage = (page) => dispatch(Actions.setItemPage(page));
    const createData = (name, calories, fat, carbs, protein) => {
        return { name, calories, fat, carbs, protein };
    }
    const [initialize, setInitialize] = React.useState(true);
    const [search, setSearch] = React.useState('');
    const [triggered, setTriggered] = React.useState(false);
    const [state, setState] = React.useState(true);
    const { authTokens } = useAuth();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);
    const [total, setTotal] = React.useState(0);
    const { itemList, itemFilter, itemCount, itemRow, itemPage } = useSelector(state => ({
        itemList: state.itemReducer.itemData.items,
        itemFilter: state.itemReducer.filter,
        itemCount: state.itemReducer.itemData.count,
        itemRow: state.itemReducer.filter.pagination.size,
        itemPage: state.itemReducer.filter.pagination.page,
    }));
    const base64Snippet = 'data:image/png;base64,';

    const handleChangePage = (event, newPage) => {
        setInitialize(false);
        setPage(newPage);
        setItemPage(newPage * itemRow);

    };


    const handleFilterSearch = (event) => {
        // setInitialize(false);
        setTriggered(true);
        if (event.keyCode == 13) {
            addItemFilterLike("name", search);
        }
        console.log(triggered);
        //await new Promise((resolve) => setTimeout(resolve, 2000));
        if (!triggered) {
            setTimeout(() => {
                addItemFilterLike("name", search);
            }, 800);
        }

    }

    const handleSearch = (event) => {
        setSearch(event.target.value);
        setTriggered(false);
        //handleFilterSearch(event.target.value);
    }

    const handleChangeRowsPerPage = (event) => {
        setInitialize(false);
        setItemPage(0);
        setPage(0);
        setItemSize(event.target.value);
    };


    useEffect(() => {
        (async () => {
            fetchAllItem(authTokens, itemFilter);
        })();

        async () => {
            dispatch(Actions.initializeItem());
        };
    }, [itemFilter]);

    const [open, setOpen] = React.useState(false);
    const [rowCollapse, setRowCollapse] = React.useState('');
    const [checked, setChecked] = React.useState([]);
    const [checkedId, setCheckedId] = React.useState([]);
    const collapseRow = (row) => {
        if (rowCollapse === row) {
            setOpen(false);
        } else {
            setOpen(true);
            setRowCollapse(row);
        }
    }


    const handleToggle = (value) => () => {
        const { itemAddHandler, itemRemoveHandler,priceHandler } = props;
        const currentIndex = checked.indexOf(value);
        const newChecked = [...checked];
        const newcheckedId = [...checkedId];
        var totalPrice = 0;
        if (currentIndex === -1) {
            totalPrice = total + parseFloat(value.price);
            newChecked.push(value);
            newcheckedId.push(value.id);
            itemAddHandler(value.id);
        } else {
            newChecked.splice(currentIndex, 1);
            newcheckedId.splice(currentIndex, 1);
            totalPrice = total - parseFloat(value.price);
            itemRemoveHandler(currentIndex);
        }
        setTotal(totalPrice);
        setChecked(newChecked);
        setCheckedId(newcheckedId);
        priceHandler(totalPrice);
        //  itemAddHandler("itemIds", checkedId);
    };




    //View Item Modal  <!--Start-->
    const [itemModalState, setItemModalState] = React.useState(false);
    const [itemDetails, setItemDetails] = React.useState();
    const openItemModal = (index) => {
        setItemDetails(itemList[index]);
        setItemModalState(true);
    };
    const closeItemModal = () => {
        setItemModalState(false);
    };
    //View Item Modal  <!--End-->

    const addPrice = (price) => {
        const priceFloat = total + parseFloat(price);
        setTotal(priceFloat);
    };




    const renderTable = () => {
        return (<TableContainer className={classes.tableContainer} component={Paper}>
            <Table stickyHeader aria-label="sticky table">
                <TableHead>
                    <TableRow>
                        <TableCell size="small" padding="checkbox"></TableCell>
                        <TableCell size="small" padding="checkbox"></TableCell>
                        <TableCell align="left">Item Name</TableCell>
                        <TableCell align="center">Type</TableCell>
                        <TableCell align="center">Price</TableCell>
                        <TableCell align="center">Status</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>

                    {checked ? checked.map((item) => (

                        <React.Fragment>

                            <TableRow key={item.id}>
                                <TableCell size="small" padding="checkbox">
                                    <Checkbox size="small"
                                        inputProps={{ 'aria-label': 'primary checkbox' }}
                                    />
                                </TableCell>
                                <TableCell size="small" padding="checkbox">
                                    <IconButton aria-label="expand row" size="small" onClick={() => collapseRow(item.id)}>
                                        {(rowCollapse === item.id) && (open) ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                                    </IconButton>
                                </TableCell>
                                <TableCell component="th" scope="row">
                                    {item.name}
                                </TableCell>
                                <TableCell align="center">{item.category.name}</TableCell>
                                <TableCell align="center">{item.price}</TableCell>
                                <TableCell align="center">{item.isActive}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                                    <Collapse node={item.id}
                                        in={(rowCollapse === item.id) && (open)} timeout="auto" unmountOnExit>
                                        <Grid container spacing={3}>
                                            <Grid xs={4} item>

                                                <div className={classes.gridImage}>
                                                    <GridList className={classes.gridList} cols={2}>
                                                        {item.imagesBase64.map((tile, index) => (
                                                            <GridListTile key={base64Snippet + tile}>
                                                                <img src={base64Snippet + tile} alt={tile.title} />

                                                            </GridListTile>
                                                        ))}
                                                    </GridList>
                                                </div>


                                                {/* <div className={classes.imagePreviewItem}>
                                                    <img alt="complex" src={base64Snippet + item.imagesBase64[0]} />
                                                </div> */}
                                            </Grid>
                                            <Grid item xs={8} sm container>
                                                {item.description}
                                            </Grid>
                                        </Grid>
                                    </Collapse>
                                </TableCell>
                            </TableRow>
                        </React.Fragment>
                    )) : <span>null</span>}
                </TableBody>
            </Table>

            <TableFooter>
                <TableRow>
                    <TablePagination
                        rowsPerPageOptions={[5, 10, 15, { value: itemCount, label: 'All' }]}
                        component="div"
                        count={itemCount}
                        rowsPerPage={itemRow}
                        page={page}
                        onChangePage={handleChangePage}
                        onChangeRowsPerPage={handleChangeRowsPerPage}
                    />
                </TableRow>
            </TableFooter>

        </TableContainer >);
    }


    const renderSelectTable = () => {
        return (<React.Fragment><TableContainer className={classes.tableContainer} >
            <Table stickyHeader aria-label="sticky table">
                <TableHead>
                    <TableRow>
                        <TableCell size="small" padding="checkbox"></TableCell>
                        <TableCell align="left">Item Name</TableCell>
                        <TableCell align="center">Type</TableCell>
                        <TableCell align="center">Price</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>

                    {checked.length !== 0 ? checked.map((item) => (
                        <React.Fragment>
                            <StyledTableRow key={item.id}>
                                <TableCell size="small" padding="checkbox">
                                    <IconButton aria-label="expand row" size="small" onClick={handleToggle(item)}>
                                        <RemoveIcon />
                                    </IconButton>
                                </TableCell>

                                <TableCell component="th" scope="row">
                                    {item.name}
                                </TableCell>
                                <TableCell align="center">{item.category.name}</TableCell>
                                <TableCell align="center"><NumberFormat value={item.price} displayType={'text'} thousandSeparator={true} prefix="₱" /></TableCell>
                            </StyledTableRow>

                        </React.Fragment>
                    )) : <TableRow><TableCell component="div" className={classes.tableContainer}>test</TableCell></TableRow>}
                </TableBody>
            </Table>



        </TableContainer>
            <Grid container spacing={2}>
                <Grid item xs={6}>
                    <Typography component="p" variant="p" className={classes.productItemTotalDisclaimer}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</Typography>
                </Grid>
                <Grid item xs={6}><TableContainer>
                    <Table size="small" aria-label="a dense table"> <TableFooter>
                        <TableRow>
                            <TableCell colSpan={2}>Quantity</TableCell>
                            <TableCell align="right">{checked.length}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell colSpan={3}>Additional Charge</TableCell>

                        </TableRow>
                        <AdditionalChargeRow>
                            <TableCell colSpan={2} align="right">Transportation</TableCell>
                            <TableCell align="right">{111}</TableCell>
                        </AdditionalChargeRow>
                        <AdditionalChargeRow>
                            <TableCell colSpan={2} align="right">Transportation</TableCell>
                            <TableCell align="right">{111}</TableCell>
                        </AdditionalChargeRow>
                        <AdditionalChargeRow>
                            <TableCell colSpan={2} align="right">Transportation</TableCell>
                            <TableCell align="right">{111}</TableCell>
                        </AdditionalChargeRow>
                        <TotalRow>
                            <TableCell colSpan={2}>Total</TableCell>
                            <TableCell align="right">{<NumberFormat value={total} displayType={'text'} thousandSeparator={true} prefix="₱" />}</TableCell>
                        </TotalRow>
                    </TableFooter>  </Table></TableContainer></Grid>
            </Grid>
            {/* <TableContainer>
                <Table size="small" aria-label="a dense table">
                    <TableHead>
                        <TableRow>
                            <TableCell >
                              
                            </TableCell>
                            <TableCell>

                            </TableCell>
                        </TableRow>
                    </TableHead>
                </Table> </TableContainer> </React.Fragment>); */}
        </React.Fragment>);
    }



    const renderComponent = () => {
        return (

            <Grid container spacing={2}
                container
                direction="row"
                justify="space-between"
                alignItems="center">
                <Grid item xs={12}>
                    <FieldName item xs={4}>
                        <Typography className={classes.fieldName} variant="h5" component="span">{props.fieldName}</Typography>
                        <Typography className={classes.fieldDescription} variant="span" component="div">{props.description}</Typography>
                    </FieldName>
                </Grid>

                <Grid item xs={6}>
                    <Grid container spacing={2} alignItems="flex-end">

                        <Grid item xs={12}>
                            <Grid container spacing={1} alignItems="flex-end">
                                <Grid item>
                                    <TextField dense value={search} onChange={handleSearch}
                                        onKeyUp={(event) => {
                                            handleFilterSearch(event, event.target.value);
                                        }}
                                        id="standard-secondary" label="Search" color="secondary" />
                                </Grid>
                                <Grid item>
                                    <SearchIcon />
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={12}>
                            <GridList cellHeight={160} cols={2} className={classes.gridList}>
                                {itemList.map((tile, index) => (
                                    <GridListTile key={base64Snippet + tile} cols={tile.featured ? 2 : 1} rows={tile.featured ? 2 : 1}>
                                        {
                                            tile.images[0] !== undefined ? <img onClick={handleToggle(tile)} alt="complex" src={`${url.MAIN_SERVICE}/image/bucket-image/${tile.images[0].imageId}`} /> : <img />
                                        }
                                        <GridListTileBar
                                            title={tile.name}
                                            titlePosition="top"
                                            actionIcon={
                                                <FormControlLabel className={classes.gridCheckBox}
                                                    control={
                                                        <Checkbox
                                                            checked={checked.indexOf(tile) !== -1}
                                                            icon={<CheckCircleOutlineIcon style={{ color: 'white' }} />}
                                                            checkedIcon={<CheckCircleIcon style={{ color: '#4caf50' }} />}
                                                        />
                                                    }
                                                />
                                            }
                                            actionPosition="left"
                                            className={classes.titleBar}
                                        />

                                        <GridListTileBar
                                            subtitle={<NumberFormat value={tile.price} displayType={'text'} thousandSeparator={true} prefix="₱" />}
                                            actionIcon={
                                                <IconButton onClick={() => openItemModal(index)} aria-label={`info about ${tile.title}`} className={classes.icon}>
                                                    <InfoIcon />
                                                </IconButton>
                                            }
                                            actionPosition="right"
                                            className={classes.gridFooterBar}
                                        />
                                    </GridListTile>
                                ))}
                            </GridList>
                            <TableFooter>
                                <TableRow>
                                    <TablePagination
                                        rowsPerPageOptions={[5, 10, 15, { value: itemCount, label: 'All' }]}
                                        component="div"
                                        count={itemCount}
                                        rowsPerPage={itemRow}
                                        page={page}
                                        onChangePage={handleChangePage}
                                        onChangeRowsPerPage={handleChangeRowsPerPage}
                                    />
                                </TableRow>
                            </TableFooter>
                        </Grid>
                    </Grid>

                </Grid>
                <Grid item xs={6}>
                    {renderSelectTable()}
                </Grid>
                <JabaModal closeModalHandler={closeItemModal}
                    openModalHandler={openItemModal}
                    modalState={itemModalState}><ItemDetailedView item={itemDetails} />
                </JabaModal>
            </Grid >);


    }

    return (itemList ? renderComponent() :
        <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
        >
            <Grid item xs={12}>
                <CircularProgress color="secondary" />
            </Grid>



        </Grid>
    )
}


export default ItemTable
