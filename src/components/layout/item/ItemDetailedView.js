import React from 'react'
import PropTypes from 'prop-types'
import { Grid } from '@material-ui/core'
import Paper from '@material-ui/core/Paper';
import { makeStyles, ThemeProvider, withStyles, useTheme } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import MobileStepper from '@material-ui/core/MobileStepper';
import Button from '@material-ui/core/Button';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import NumberFormat from 'react-number-format';
import JabaTheme from '../../../theme/JabaTheme';
import Chip from '@material-ui/core/Chip';
import Divider from '@material-ui/core/Divider';
const AutoPlaySwipeableViews = autoPlay(SwipeableViews);
const useStyles = makeStyles((JabaTheme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: JabaTheme.palette.background.paper,


    },
    gridList: {
        width: 500,
        height: 450,
        transform: 'translateZ(0)',

    },
    divider: {
        margin: '20px 0px',
    },
    dividerContainer: {
        padding: '0px 25px'
    }
}));


const imageStepper = makeStyles((JabaTheme) => ({
    root: {
        maxWidth: 500,
        flexGrow: 1,
        margin: '0 auto',
        '& div[class*="MuiMobileStepper-root"]': {
            backgroundColor: 'transparent'
        },
        '& div[class*="MuiMobileStepper-dots"]': {
            margin: '0 auto'
        }
    },
    header: {
        display: 'flex',
        alignItems: 'center',
        height: 50,
        paddingLeft: JabaTheme.spacing(4),
        backgroundColor: JabaTheme.palette.background.default,
    },
    img: {
        height: 300,
        display: 'block',
        maxWidth: 500,
        overflow: 'hidden',
        width: '100%',
    },
}));


const cardStyle = makeStyles((JabaTheme) => ({
    root: {
        maxWidth: 500,
    },
}));

const chipStyleClass = makeStyles((JabaTheme) => ({
    root: {
        display: 'flex',
        justifyContent: 'center',
        flexWrap: 'wrap',
        listStyle: 'none',
        padding: JabaTheme.spacing(0.5),
        margin: 0,

    },
    chip: {
        margin: '5px 5px',
    },
}));

const SupplierName = withStyles({
    root: {
        display: 'block',
        marginBottom: '5px',
        fontSize: '10pt',
        fontWeight: '200',
    },
})(Typography);

export const ItemDetailedView = (props) => {
    const classes = useStyles();
    const imageStepperClass = imageStepper();
    const cardStyleClass = cardStyle();
    const chipStyle = chipStyleClass();
    const renderImageViewer = () => {
        const [activeStep, setActiveStep] = React.useState(0);
        const maxSteps = props.item.imagesBase64.length;

        const base64Snippet = 'data:image/png;base64,';

        const handleNext = () => {
            setActiveStep((prevActiveStep) => prevActiveStep + 1);
        };

        const handleBack = () => {
            setActiveStep((prevActiveStep) => prevActiveStep - 1);
        };

        const handleStepChange = (step) => {
            setActiveStep(step);
        };
        const theme = useTheme();
        return (<div className={imageStepperClass.root}>

            <AutoPlaySwipeableViews
                axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                index={activeStep}
                onChangeIndex={handleStepChange}
                enableMouseEvents
            >
                {props.item.imagesBase64.map((step, index) => (
                    <div key={step.label}>
                        {Math.abs(activeStep - index) <= 2 ? (
                            <img className={imageStepperClass.img} src={base64Snippet + step} />

                        ) : null}
                    </div>
                ))}
            </AutoPlaySwipeableViews>
            <MobileStepper
                steps={6}
                position="static"
                variant="dots"
                activeStep={activeStep}

            />
        </div>)
    }


    return (
        <ThemeProvider theme={JabaTheme}>
            <Card className={cardStyleClass.root}>
                <CardActionArea>
                    <CardMedia>{renderImageViewer()}</CardMedia>
                    <CardContent component={Paper}>
                        <Grid   spacing={3} container
                            direction="row"
                            justify="center"
                            alignItems="center">
                            <Grid item xs={9}>
                                <Typography gutterBottom variant="h3" component="span">
                                    {props.item.name}
                                </Typography>
                                <SupplierName variant="span" component="span">
                                    {props.item.category.name}
                                </SupplierName>
                            </Grid>
                            <Grid item xs={3}>   <Typography gutterBottom variant="h4" component="span">
                                <NumberFormat value={props.item.price} displayType={'text'} thousandSeparator={true} prefix="₱" />

                            </Typography>
                            </Grid>
                        </Grid>

                        <Typography variant="body2" color="textSecondary" component="p">
                            {props.item.description}
                        </Typography>
                        <Grid spacing={0} container
                            direction="row"
                            justify="center"
                            alignItems="center" className={classes.dividerContainer}>
                            <Grid item xs={12}>
                                <Divider className={classes.divider} variant="middle" />
                            </Grid>
                       
                            <Grid item xs={12}>
                                <Typography gutterBottom variant="h5" component="span">
                                    Hash Tags
                                </Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <Paper elevation={0} component="div" className={chipStyle.root}>
                                    {props.item.hashTags.map((data) => {
                                        return (
                                            <Chip
                                                size='small'
                                                label={data.hashtag}
                                                className={chipStyle.chip}
                                            />
                                        );
                                    })}
                                </Paper>
                            </Grid>
                        </Grid>

                    </CardContent>
                </CardActionArea>
                {/* <CardActions>
                    <Button size="small" color="primary">
                        Share
          </Button>
                    <Button size="small" color="primary">
                        Learn More
          </Button>
                </CardActions> */}
            </Card>
        </ThemeProvider>


        // <Grid container component={Paper}>
        //     <Grid item xs={12} >

        //             {/* <GridList cellHeight={160} className={classes.gridList} cols={2}>
        //                 {props.item.imagesBase64.map((tile) => (
        //                     <GridListTile key={base64Snippet + tile} rows={tile.featured ? 2 : 1}
        //                         cols={tile.featured ? 2 : 1}>
        //                         <img src={base64Snippet + tile} />
        //                     </GridListTile>
        //                 ))}
        //             </GridList> */}{renderImageViewer()}

        //     </Grid>
        //     <Grid item xs={12}>Test</Grid>

        // </Grid>


    )
}

ItemDetailedView.propTypes = {
    props: PropTypes
}


export default ItemDetailedView;
