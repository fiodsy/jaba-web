import React from 'react';
import PropTypes from 'prop-types';
import PlainPanel from '../../layout/panel/PlainPanel/PlainPanel';
import PlainSection from '../../layout/panel/PanelSection/PlainSection';
import Grid from '@material-ui/core/Grid';
import JabaTextField from '../../fields/JabaTextField/JabaTextField';
import { useDispatch, useSelector } from 'react-redux';
import TextArea from '../../fields/TextArea/TextArea';
import * as Actions from '../../../redux/action';
import { useAuth } from '../../context/auth';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import SelectField from '../../fields/Select/SelectField';
import ImageUploadAdvance from '../../fields/ImageUploadAdvance/ImageUploadAdvance';
import AmountField from '../../fields/AmountField/AmountField';
import ChipArrayField from '../../fields/ChipArrayField/ChipArrayField';
import * as ApiService from '../../../api/ApiService';
import Button from '@material-ui/core/Button';
const ItemCreateForm = props => {
    const { itemTypes, productCategories, productItem, itemImages } = useSelector(state => ({
        itemTypes: state.lookUpReducer.itemTypes,
        productCategories: state.lookUpReducer.productCategories,
        productItem: state.itemReducer.data,
        itemImages: state.itemReducer.data.base64Images
     
    }));
    const { authTokens, userId } = useAuth();
    const dispatch = useDispatch();
    const updateFieldValue = (name, value) => dispatch(Actions.fieldChangeHandler(name, value));
    const addHashtag = (value) => dispatch(Actions.addHashTag(value));
    const removeHashtag = (index) => dispatch(Actions.removeHashTag(index));
    const fieldChangeHandler = (event) => {
        const value = event.target.value;
        updateFieldValue([event.target.name], value);
    }

    const onClickSubmit = (event) => {
        event.preventDefault();
        dispatch(Actions.itemFieldChangeHandler("base64Images", itemImages));
        ApiService.createItem(productItem, authTokens).then(async response => {
            if (response.status === 200) {
                alert('IN');
            }
        });

    };

    const updateProductItem = (event) => {
        const value = event.target.value;
        dispatch(Actions.itemFieldChangeHandler([event.target.name], value));
    }

    const addItemImage = (image) => {
        dispatch(Actions.addItemImage(image));
    }

    const removeItemImage = (index) => {
        dispatch(Actions.removeItemImage(index));
    }


    return (
        <PlainPanel panelName='Item Details'>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <PlainSection >
                        <JabaTextField fieldSize="35" fieldStyle="hor" onChange={updateProductItem} fieldName="Name" name="name" description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
XXX px x XXX px"></JabaTextField>
                        <TextArea fieldSize="350" fieldStyle="hor" onChange={updateProductItem} fieldName="Description" name="description"></TextArea>
                        <SelectField onChange={updateProductItem} fieldStyle="hor" choices={itemTypes} fieldName="Item Type" name="itemCode" description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
XXX px x XXX px"></SelectField>
                        <ImageUploadAdvance imageList={itemImages} removeImageHandler={removeItemImage} addImageHandler={addItemImage} fieldStyle="hor" fieldName="Images" description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
XXX px x XXX px"></ImageUploadAdvance>
                        <AmountField fieldStyle="hor" onChange={updateProductItem} fieldName="Price" name="price" description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
XXX px x XXX px"></AmountField>
                        <ChipArrayField addHandler={addHashtag} fieldStyle="hor" dataHandler={productItem.hashtags} removeHandler={removeHashtag} fieldName="Hashtags" name="hashtags" description="Lorem ipsum dolor sit amet, consectetur adipiscing elit." ></ChipArrayField>
                        <form noValidate >
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                onClick={onClickSubmit}
                            >
                                Submit
              </Button>

                        </form>
                    </PlainSection>
                </Grid>
            </Grid>
        </PlainPanel>
    );
};

ItemCreateForm.propTypes = {

};

export default ItemCreateForm;