import React, { useState } from 'react';
import PropTypes from 'prop-types';
import PlainPanel from '../../layout/panel/PlainPanel/PlainPanel';
import PlainSection from '../../layout/panel/PanelSection/PlainSection';
import Grid from '@material-ui/core/Grid';
import JabaTextField from '../../fields/JabaTextField/JabaTextField';
import TextArea from '../../fields/TextArea/TextArea';
import ImageUploadField from '../../fields/ImageUpload/ImageUploadField';
import AddressField from '../../fields/Address/AddressField';
import { useAuth } from '../../context/auth';
import DocumentField from '../../fields/Document/DocumentField';
import ContactNumberField from '../../fields/ContactNumber/ContactNumberField';
import { useDispatch, useSelector } from 'react-redux';
import Button from '@material-ui/core/Button';
import * as Actions from '../../../redux/action';
import * as ApiService from '../../../api/ApiService';
import { Divider } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
const DividerCustom = withStyles({
    root: {
        marginTop: '20px',
        marginBottom: '20px',
    },
})(Divider);

const OrganizationCreateForm = (props) => {

    const { authTokens, userId } = useAuth();

    const organizationData = useSelector(state => ({ 
        name: state.jabaTextFieldReducer.fieldData.organizationName,
        base64Logo: state.jabaTextFieldReducer.fieldData.base64Logo,
        address: state.addressReducer.address,
        contact: {
            contactInformations: state.contactNumberReducer.contactInformations,
            socialLinks: [
                {
                    "key": "facebook",
                    "value": "string"
                }
            ]
        },
        documentResponse: state.documentUploadReducer.documentResponse,
        textContents: [state.jabaTextFieldReducer.fieldData.textContents]

    }));
    const dispatch = useDispatch();
    const addressData = useSelector(state => state.addressReducer.address);

    const updateFieldValue = (name, value) => dispatch(Actions.fieldChangeHandler(name, value));
    const fieldChangeHandler = (event) => {
        const value = event.target.value;
        updateFieldValue([event.target.name], value);
    }



    const onClickTest = (event) => {
        event.preventDefault();
        console.log(JSON.stringify(organizationData));
        ApiService.createOrganization(organizationData, userId, authTokens).then(async response => {
            if (response.status === 200) {
                alert('IN');

            }
        });

    };



    return (
        <PlainPanel panelName='Organization Details'>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <PlainSection>
                        <ImageUploadField fieldStyle="ver" fieldName="Organization Logo" description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
XXX px x XXX px"></ImageUploadField>
                        <JabaTextField fieldStyle="ver" fieldSize="25" onChange={fieldChangeHandler} fieldName="Name" name="organizationName" description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
XXX px x XXX px"></JabaTextField>
                        <TextArea fieldStyle="ver" fieldSize="350" onChange={fieldChangeHandler} fieldName="Company Description" name="textContents"></TextArea>
                        <DividerCustom variant="middle" />
                        <ContactNumberField fieldStyle="ver" fieldName="Contact Number" description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
XXX px x XXX px"></ContactNumberField>
                        <AddressField fieldStyle="ver"></AddressField>
                        <DividerCustom variant="middle" />
                        <DocumentField fieldStyle="hor" fieldName="Scanned Documents" description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
XXX px x XXX px"></DocumentField>
                        <form noValidate >
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                onClick={onClickTest}
                            >
                                Submit
              </Button>

                        </form>
                    </PlainSection>
                </Grid>
            </Grid>



        </PlainPanel>

    );
};



OrganizationCreateForm.propTypes = {

};




export default OrganizationCreateForm;