import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import image from '../../../../assets/profileimage/me.jpg';
import { Typography } from '@material-ui/core';
import classes from '../avatar/UserAvatar.css';
import Badge from '@material-ui/core/Badge';
import IconButton from '@material-ui/core/IconButton';
import NotificationsIcon from '@material-ui/icons/Notifications';
import { withStyles, useStyles } from '@material-ui/core/styles';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
const UserAvatar = props => {


    const StyledBadge = withStyles((theme) => ({
        badge: {
            right: -3,
            top: 13,
            border: `2px solid ${theme.palette.background.paper}`,
            padding: '0 4px',
        },
    }))(Badge);


    return (
        
        // <Grid
        //     container
        //     direction="row"
        // >

        //     <Grid item xs={12}>
                <List dense className={classes.listAvatar}>
                <ListItem>
                    <ListItemAvatar>
                        
                            <Avatar src={image} />
                        
                    </ListItemAvatar>
                    <ListItemText>
                        <Typography className={classes.hiMessage}>Hi, Seo</Typography>
                    </ListItemText>


                    <ListItemSecondaryAction>
                        <IconButton aria-label="cart">
                            <StyledBadge badgeContent={4} color="secondary">
                                <NotificationsIcon />
                            </StyledBadge>
                        </IconButton>
                    </ListItemSecondaryAction>
                    </ListItem>
                </List>

        //     </Grid>
        // </Grid>
    );
};

// UserAvatar.propTypes = {

// };

export default UserAvatar;