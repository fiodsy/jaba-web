import React from 'react';
import classes from '../announcement/AnnouncementStripStyle.css'
// import PropTypes from 'prop-types';
import announceicon from '../../../../assets/icon/announceicon.png'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { withStyles } from '@material-ui/core/styles';


const AnnouncementStrip = props => {

    const AnnouncementItem = withStyles({
        root: {
            '&.MuiList-padding': {
                padding: '0',
                marginLeft:'25px'
            },
            '& li.MuiListItem-root':{
                padding: '0',
            },
            '& .MuiListItemIcon-root img':{
                padding: '0 21px',
            },
            '& .MuiListItemText-root':{
                margin: '0px 25px'
            },
            '& .MuiTypography-body1': {
                fontSize: '11pt',
                color: '#fff',
                fontWeight: '400'
            }

        },
    })(List);
    return (
        <div className={classes.Strip}>
            <AnnouncementItem>
                <ListItem >
                    <ListItemIcon>
                        <img src={announceicon} alt="accnounceicon" />
                    </ListItemIcon>
                    <ListItemText primary="Lorem ipsum dolor sit amet, consectetur adipiscing elit, ek..." />
                </ListItem>
            </AnnouncementItem>
        </div>
    );
};

// AnnouncementStrip.propTypes = {

// };

export default AnnouncementStrip;