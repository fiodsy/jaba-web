import React from 'react';
import PropTypes from 'prop-types';
import logo from '../../../../assets/logo/logo_badge.png';
import classes from '../logo/logobadge.css'
import Grid from '@material-ui/core/Grid';

const HeaderLogoBadge = props => {
    return (
        <Grid className={classes.Logo} container>
            <Grid md>
                <img className={classes.LogoImage}   src={logo} alt="Logo" />
            </Grid>
        </Grid>
    );
};

HeaderLogoBadge.propTypes = {

};

export default HeaderLogoBadge;