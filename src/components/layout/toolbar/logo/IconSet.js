import React from 'react';
import fb from '../../../../assets/logo/fb_icon.png';
import google from '../../../../assets/logo/google_icon.png';


const IconSet = (props) => {
    switch (props.name) {
        case 'facebook': return <img src={fb} alt="Logo" />
        case 'google': return <img src={google} alt="Logo" />
    }
};

export default IconSet;