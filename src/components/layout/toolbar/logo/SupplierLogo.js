import React from 'react';
import PropTypes from 'prop-types';
import logo from '../../../../assets/logo/supplier_logo.png';
import Grid from '@material-ui/core/Grid';
import classes from '../logo/SupplierLogo.css'
const SupplierLogo = props => {
    return (
        <Grid className={classes.Logo} container>
        <Grid md>
            <img className={classes.LogoImage}   src={logo} alt="Logo" />
        </Grid>
    </Grid>
    );
};

// SupplierLogo.propTypes = {
    
// };

export default SupplierLogo;