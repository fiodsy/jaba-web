import React from 'react';
import PropTypes from 'prop-types';
import NameAndViewAllPanel from '../../layout/panel/NameAndViewAllPanel/NameAndViewAllPanel';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import SimpleCard from '../../card/SimpleCard';
import RecentReviewListItem from '../../list/RecentReviewListItem/RecentReviewListItem';
import List from '@material-ui/core/List';
import sampleProduct1 from '../../../assets/samples/_TP_0967.jpg';
import sampleProduct2 from '../../../assets/samples/Sounds_02.jpg';
import sampleProduct3 from '../../../assets/samples/Cakes_02.jpg';
import classes from '../home/DashboardComponent.css';
import Typography from '@material-ui/core/Typography';
import JabaTheme from '../../../theme/JabaTheme';
import globalclass from '../../../index.css';
import { makeStyles, ThemeProvider, withStyles } from '@material-ui/core/styles';
const DashboardComponent = props => {
    return (
        <ThemeProvider theme={JabaTheme}>

            <Grid item xs={12}>
                <Container maxWidth="lg">
                    <Grid container>
                        <Grid xs={12}>
                            <main className={globalclass.content}>
                                <Grid container spacing={2}>
                                    <Grid item xs={8}>
                                        <NameAndViewAllPanel panelName='Featured Product'>
                                            <Grid container spacing={2}>
                                                <Grid item xs={4}>
                                                    <SimpleCard description='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod' productName={'Photography Package'} imageUrl={sampleProduct1}></SimpleCard>
                                                </Grid>
                                                <Grid item xs={4}>
                                                    <SimpleCard description='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod' productName={'Sound Machine'} imageUrl={sampleProduct2}></SimpleCard>
                                                </Grid>
                                                <Grid item xs={4}>
                                                    <SimpleCard description='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod' productName={'Wedding Cake'} imageUrl={sampleProduct3}></SimpleCard>
                                                </Grid>

                                            </Grid>
                                        </NameAndViewAllPanel>
                                    </Grid>
                                    <Grid item xs={4}>
                                        <NameAndViewAllPanel panelName='Recent Reviews'>
                                            <List>
                                                <RecentReviewListItem></RecentReviewListItem>
                                                <RecentReviewListItem></RecentReviewListItem>
                                                <RecentReviewListItem></RecentReviewListItem>
                                                <RecentReviewListItem></RecentReviewListItem>
                                                <RecentReviewListItem></RecentReviewListItem>
                                            </List>
                                        </NameAndViewAllPanel>
                                    </Grid>
                                    <Grid item xs={8}>
                                        <NameAndViewAllPanel>
                                            <Grid container spacing={2}>
                                                <Grid item xs={4}>
                                                    <SimpleCard></SimpleCard>
                                                </Grid>
                                                <Grid item xs={4}>
                                                    <SimpleCard></SimpleCard>
                                                </Grid>
                                                <Grid item xs={4}>
                                                    <SimpleCard></SimpleCard>
                                                </Grid>


                                            </Grid>
                                        </NameAndViewAllPanel>
                                    </Grid>
                                </Grid>



                            </main>
                        </Grid>
                    </Grid>
                </Container>
            </Grid>

        </ThemeProvider>
    );
};

DashboardComponent.propTypes = {

};

export default DashboardComponent;