import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles, ThemeProvider, withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import logo from '../../../assets/logo/FB_DP.jpg';
import JabaTheme from '../../../theme/JabaTheme';
import IconSet from '../../../components/layout/toolbar/logo/IconSet';
import Divider from '@material-ui/core/Divider';

const CssTextField = withStyles({
    root: {
        backgroundColor: '#f4f4f4',
        '& label': {
            fontSize: '0.8rem'
        },
        '& label.Mui-focused': {
            color: '#d92424',
            backgroundColor: '#fff',
        },
        '& .MuiInput-underline:after': {
            borderBottomColor: 'green',
        },
        '& .MuiOutlinedInput-root': {
            '&.Mui-focused': {
                backgroundColor: '#fff',
            },
            '& fieldset': {
                borderColor: '#e2e2e2',
                borderBottom: '2px solid #e2e2e2',
                boxShadow: '0px 1px 2px 0px #e2e2e2',
            },
            '&:hover fieldset': {
                borderColor: '#a8a8a8',
            },
            '&.Mui-focused fieldset': {
                border: '1px solid #d92424',
            },
        },
    },
})(TextField);

const SubmitButton = withStyles({
    root: {

        '&.MuiButton-root': {
            margin: '0px'
        }
    },
})(Button);

const BootstrapButton = withStyles({
    root: {
        boxShadow: 'none',
        textTransform: 'none',
        fontSize: 12,
        color: '#fff',
        padding: '6px 12px',
        border: '1px solid',
        lineHeight: 1.5,
        borderColor: '#0063cc',

        '&:hover': {
            backgroundColor: '#0069d9',
            borderColor: '#0062cc',
            boxShadow: 'none',
        },
        '&:active': {
            boxShadow: 'none',
            backgroundColor: '#0062cc',
            borderColor: '#005cbf',
        },
        '&:focus': {
            boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
        },
    },
})(Button);


const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(25),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: '#fff',
        padding: '25px',
        borderRadius: 5,
        boxShadow: '0px 3px 2px -1px #464646'
    },
    avatar: {
        margin: theme.spacing(1),
        height: '70px',
        width: '70px'
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    signintext: {
        color: '#d92424'
    },
    smallText: {
        fontSize: '0.8rem',
        color: '#464646'
    },
    fbButton: {
        backgroundColor: '#4267b2',
        color: '#fff'
    },
    googleButton: {
        backgroundColor: '#f8f8f8',
        color: '#595959',
        borderColor: '#e2e2e2',
        '&:hover': {
            backgroundColor: '#f2f2f2',
            borderColor: '#d3d3d3',
            boxShadow: 'none',
        }
    },
    or: {
        fontSize: '0.7rem',
        color: '#464646'
    },
    dividerMargin: {
        margin: '10px 0px'
    }


}));

const Copyright = (props) => {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://material-ui.com/">
                JABA
          </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}


const RegistrationComponent = (props) => {
    const classes = useStyles();
    return (

        <Container component="main" maxWidth="xs">
            <ThemeProvider theme={JabaTheme}>
                <div className={classes.paper}>

                    <Grid container spacing={3}>
                        <Grid item xs={12}>
                            <span className={classes.signintext}>REGISTRATION</span>
                        </Grid>
                    </Grid>
                    <form className={classes.form} noValidate onSubmit={props.submit}>
                        <CssTextField
                            onChange={props.emailChange}
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Email Address"
                            name="email"
                            autoComplete="email"
                            autoFocus
                        />
                        <CssTextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                            onChange={props.passwordChange}
                        />
                        <CssTextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="confirmpassword"
                            label="Confirm Password"
                            type="confirmpassword"
                            id="confirmpassword"
                            autoComplete="current-password"
                        />
                        <Grid className={classes.dividerMargin} container>
                            <Grid xs>
                                <Typography className={classes.smallText}>Please tick the box to continue:
</Typography>

                                <SubmitButton
                                    type="submit"
                                    fullWidth
                                    variant="contained"
                                    color="primary"
                                    className={classes.submit}
                                >
                                    Submit
              </SubmitButton>
                            </Grid>

                        </Grid>

                    </form>
                </div>
                <Box mt={8}>
                    <Copyright />
                </Box>
            </ThemeProvider>
        </Container>

    );
};

// LoginComponent.propTypes = {

// };

export default RegistrationComponent;