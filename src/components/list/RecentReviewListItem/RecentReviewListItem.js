import React from 'react';
import PropTypes from 'prop-types';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import sampleImage from '../../../assets/samples/_TP_0967.jpg';
import RatingStar from '../../misc/RatingStar/RatingStar';
import Grid from '@material-ui/core/Grid';
import classes from '../RecentReviewListItem/RecentReviewListItem.css'

const RecentReviewListItem = props => {

    const ReviewListItem = withStyles({
        root: {
            padding: '0px',
        },
    })(ListItem);

    const ReviewListItemAvatar = withStyles({
        root: {
            '& .MuiAvatar-root': {
                width: '90px',
                height: '90px',
                borderRadius: '5px',
                marginRight: '16px'
            }
        },
    })(ListItemAvatar);

    const SupplierName = withStyles({
        root: {
            fontSize: '10pt',
            fontWeight: '200',
        },
    })(Typography);

    const ProductName = withStyles({
        root: {
            fontSize: '13pt',
            fontWeight: '400',
        },
    })(Typography);

    const CustomerName = withStyles({
        root: {
            fontSize: '12pt',
            fontWeight: '200',
            textAlign: 'end'
        },
    })(Typography);

    const Review = withStyles({
        root: {
            fontSize: '10pt',
            fontWeight: '200',
        },
    })(Typography);

    // const RatingStar = withStyles({
    //     root: {

    //         fontSize: '12pt',
    //         color: '#ffc107',


    //     },
    // })(GradeIcon);

    return (
        <ReviewListItem alignItems="flex-start">
            <ReviewListItemAvatar>
                <Avatar variant="square" src={sampleImage} />
            </ReviewListItemAvatar>
            <ListItemText>


                <Typography
                    component="div"
                    variant="body2"
                    color="textPrimary"
                >
                    Photography Package
              </Typography>

                <Review
                    component="p"
                    variant="body2"
                    color="textPrimary"
                >
                    {" \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod temporartio acuar...\""}
                </Review>
                <Grid container
                    direction="row"
                    justify="center"
                    alignItems="center">
                    <Grid item md={6}>
                        <RatingStar></RatingStar>
                    </Grid>
                    <Grid item md={6} className={classes.customerNameGrid}>
                        <Typography
                            component="span"
                            variant="body2"
                            color="textSecondary"
                        >
                            Ali Connors
              </Typography>
                    </Grid>
                </Grid>
            </ListItemText>



        </ReviewListItem>
    );
};

// RecentReviewListItem.propTypes = {

// };

export default RecentReviewListItem;