import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Input from '@material-ui/core/Input';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import classes from '../TextField.css';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import * as Actions from '../../../redux/action';
import { useDispatch, useSelector } from 'react-redux';
import noimage from '../../../assets/background/no_image.png';
import FieldWrapper from '../FieldWrapper/FieldWrapper';
const UploadBox = withStyles({
    root: {
        padding: '8px'
    },
})(Box);

const FieldName = withStyles({
    root: {
        paddingLeft: '15px'
    },
})(Grid);
const ImageUploadField = props => {
    const dispatch = useDispatch();
    const [imagepreview, setImagePreview] = useState(noimage);
    const uploadButtonEvent = (event) => {

        var file = event.target.files[0];
        const reader = new FileReader();
        var url = reader.readAsDataURL(file);
        reader.onload = function (e) {
            // get loaded data and render thumbnail.
            document.getElementById("image").src = e.target.result;
        };
        reader.onloadend = function () {
            var base64data = reader.result;
            const chars = base64data.split(',');
            dispatch(Actions.fieldChangeHandler("base64Logo", chars[1]));
        }

    }

    const renderComponent = () => {
        return (<UploadBox className={classes.uploadBox} component="div" color="red">

            <div className={classes.uploadButton}>
                <input
                    accept="image/*"
                    className={classes.imageinput}
                    id="contained-button-file"
                    multiple
                    type="file"
                    onChange={uploadButtonEvent}
                />
                <label htmlFor="contained-button-file">
                    <Button size="small" variant="contained" color="primary" component="span">
                        Upload
                </Button>
                </label>
            </div>
            <img id="image" className={classes.imgBox} src={imagepreview} />
        </UploadBox>);
    }
    return (
        <FieldWrapper fieldName={props.fieldName} fieldStyle={props.fieldStyle} description={props.description}>
            {renderComponent()}
        </FieldWrapper>
    )
};

ImageUploadField.propTypes = {
    fieldName: PropTypes.string,
    description: PropTypes.string,
};

export default ImageUploadField;