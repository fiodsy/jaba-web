import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Input from '@material-ui/core/Input';
import TextField from '@material-ui/core/TextField';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import classes from '../TextField.css';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import SaveIcon from '@material-ui/icons/Save';
const FieldName = withStyles({
    root: {

        paddingBottom: '0px !important',
        paddingTop: '0px !important',
    },
})(Grid);

const VERTICAL = "ver";
const HORIZONTAL = "hor";
const FieldWrapper = (props) => {
    const {editHandler, disableHandler, updateHandler} = props;
    const [edit, setEdit] = React.useState(false);
    const editEventHandler = (event) => {
        editHandler(edit);
        disableHandler(edit);
        setEdit(!edit);
        if(edit===true){
            updateHandler(event);
        }
    }
    const renderField = () => {
        if (props.fieldStyle.toUpperCase() === VERTICAL.toUpperCase()) {
            return <Grid
                container
                direction="row"
                justify="center"
                alignItems="center"
                spacing={2}>
                <FieldName item xs={4}>
                    <Typography className={classes.fieldName} variant="h5" component="span">{props.fieldName}</Typography>
                    <Typography className={classes.fieldDescription} variant="span" component="div">{props.description}</Typography>
                </FieldName>

                <Grid item xs={8}>
                    <Grid item xs={12}>
                        {props.children}

                    </Grid>
                    {props.fieldSize !== undefined ?
                        <Grid container
                            direction="row"
                            justify="flex-end"
                            alignItems="flex-end" item xs={12}>
                            <Typography className={classes.fieldDescription} variant="span" component="span">{props.fieldSizeHandler}</Typography>
                        </Grid> : null
                    }
                </Grid>
            </Grid>
        } else if (props.fieldStyle.toUpperCase() === HORIZONTAL.toUpperCase()) {
            return <Grid
                container
                direction="row"
                justify="center"
                alignItems="center"
                spacing={2}>
                <FieldName item xs={12}>
                    <Grid
                        container
                        direction="row"
                        justify="center"
                        alignItems="center">
                        <Grid item xs={props.isEditField === true ? 11 : 12}>
                            <Typography className={classes.fieldName} variant="h5" component="div">{props.fieldName}</Typography>
                            <Typography className={classes.fieldDescription} variant="span" component="div">{props.description}</Typography>
                        </Grid>

                        {props.isEditField === true ?
                            <Grid item xs={1} >
                                <IconButton size="small" aria-label="edit" color="primary" onClick={(event) => editEventHandler(event)}>
                                    {edit ? < SaveIcon /> : <EditIcon />}
                                </IconButton>
                            </Grid>
                            : null}
                    </Grid>
                </FieldName>

                <Grid item xs={12}>
                    <Grid item xs={12}>
                        {props.children}

                    </Grid>
                    {props.fieldSize !== undefined ?
                        <Grid container
                            direction="row"
                            justify="flex-end"
                            alignItems="flex-end" item xs={12}>
                            <Typography className={classes.fieldDescription} variant="span" component="span">{props.fieldSizeHandler}</Typography>
                        </Grid> : null
                    }
                </Grid>
            </Grid>
        } else {
            return (props.children)
        }

    }

    return (
        <div className={classes.fieldWrapper}>
            {props.fieldStyle ? renderField() : props.children}
        </div>
    )
}

FieldWrapper.propTypes = {
    fieldName: PropTypes.string,
    description: PropTypes.string,
    fieldSize: PropTypes.number,

}

export default FieldWrapper

