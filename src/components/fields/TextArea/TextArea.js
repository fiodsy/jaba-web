import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Input from '@material-ui/core/Input';
import TextField from '@material-ui/core/TextField';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import classes from '../TextField.css';
import FieldWrapper from '../FieldWrapper/FieldWrapper';
const TextArea = props => {


    const [defaultSize, setDefaultSize] = useState(props.fieldSize);

    const fieldSizeHandler = (size) => {
        return defaultSize + "/" + size + " characters"
    }

    const textFieldOnChange = (onChangeValue, event) => {
        onChangeValue(event);
        setDefaultSize(props.fieldSize - event.target.value.length);
    }


    const renderComponent = () => {
        return (<TextField
            name={props.name}
            id="outlined-full-width"
            placeholder="Placeholder"
            multiline
            rows={4}
            fullWidth
            InputLabelProps={{
                shrink: true,
            }}
            value={props.value}
            inputProps={{
                maxLength: props.fieldSize !== undefined ? props.fieldSize : 350,
            }}
            variant="outlined"
            onChange={(event) => textFieldOnChange(props.onChange, event)} />);
    }
    return (
        <FieldWrapper fieldSizeHandler={fieldSizeHandler(props.fieldSize)} fieldSize={props.fieldSize} fieldName={props.fieldName} fieldStyle={props.fieldStyle} description={props.description}>
            {renderComponent()}
        </FieldWrapper>
    )
};

TextArea.propTypes = {
    fieldName: PropTypes.string,
    description: PropTypes.string,
    fieldSize: PropTypes.number
};

export default TextArea;