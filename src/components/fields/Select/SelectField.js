import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Input from '@material-ui/core/Input';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import classes from '../TextField.css';
import FieldWrapper from '../FieldWrapper/FieldWrapper';

const SelectField = props => {

    const renderComponent = () => {
        return (<Select

            labelId="demo-simple-select-outlined-label"
            id="demo-simple-select-outlined"
            value={props.valueHolder}
            onChange={props.onChange}
            fullWidth
            variant="outlined"
            name={props.name}
            value={props.value}
        >

            {props.choices ? props.choices.map(choice => (<MenuItem value={choice.code}>{choice.name}</MenuItem>)) :
             <MenuItem value="">
                <em>None</em>
            </MenuItem>}
        </Select>);
    }

    return (
        <FieldWrapper fieldSize={props.fieldSize} fieldName={props.fieldName} fieldStyle={props.fieldStyle} description={props.description}>
            {renderComponent()}
        </FieldWrapper>
    )
};

SelectField.propTypes = {

};

export default SelectField;