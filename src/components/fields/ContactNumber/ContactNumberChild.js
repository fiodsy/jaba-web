import React, { useState } from 'react';
import InputAdornment from '@material-ui/core/InputAdornment';
import NumberFormat from 'react-number-format';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../../redux/action';
const NumberFormatCustom = (props) => {
    const { inputRef, onChange, ...other } = props;

    return (
        <NumberFormat
            {...other}
            getInputRef={inputRef}
            onValueChange={(values) => {
                onChange({
                    target: {
                        name: props.name,
                        value: values.value,
                    }
                });
            }}
            format="(+##)-###-###-####"
            allowEmptyFormatting mask="_"
        />
    );
}

const ContactNumberChild = (props) => {

    const dispatch = useDispatch();
    const [value, setValue] = useState();
    
    const { contactInformations } = useSelector(state => ({
        contactInformations: state.contactNumberReducer.contactInformations
    }));
    const contactInformation = contactInformations[props.index];
    const updateContactInformation = (field, value, index) => dispatch(Actions.updateContactInformationByIndex(field, value, index));

    const handleChange = (event) => {
        updateContactInformation("value", event.target.value, props.index);
    };
    return (
        <TextField
            id={props.id}
            onChange={handleChange}
            value={value}
                    fullWidth
            InputLabelProps={{
                shrink: true,
            }}
            InputProps={{
                inputComponent: NumberFormatCustom,
            }}
            margin="normal"
            variant="outlined" />

    );
};

ContactNumberChild.propTypes = {

};

export default ContactNumberChild;