import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import { withStyles } from '@material-ui/core/styles';
import classes from '../TextField.css';
import ContactNumberChild from './ContactNumberChild';
import Button from '@material-ui/core/Button';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../../redux/action';
import FieldWrapper from '../FieldWrapper/FieldWrapper';
const FieldName = withStyles({
    root: {
        paddingLeft: '15px'
    },
})(Grid);

const randomString = (length) => {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}


const ContactNumberField = props => {
    const dispatch = useDispatch();

    const { fieldProps, currentIndex } = useSelector(state => ({
        fieldProps: state.contactNumberReducer.fieldProps,
        currentIndex: state.contactNumberReducer.currentIndex
    }));

    const addContactNumberField = (key, id, index) => dispatch(Actions.updateContactFieldProps({ key, id, index }));
    const updateContactInformation = (type, value) => dispatch(Actions.updateContactInformation(type, value));

    const incrementIndex = () => dispatch(Actions.incrementIndex());

    const spawnContactNumberChild = () => {
        if (currentIndex !== 3) {
            incrementIndex();
            addContactNumberField(randomString(5), randomString(8), currentIndex);
            updateContactInformation("key", "mobilenumber");
        } else {
            alert("Only 3 mobile number is allowed");
        }


    }

    const renderComponent = () => {
        return (<div>{
            fieldProps.map(fieldProp => {
                return (<ContactNumberChild id={fieldProp.id} key={fieldProp.key} index={fieldProp.index}></ContactNumberChild>)
            })}
            <Grid justify="flex-end" direction="row"
                alignItems="center" container>
                <Grid item>
                    <Button onClick={spawnContactNumberChild} color="primary">
                        + Add Another Number</Button>
                </Grid>
            </Grid></div>);
    }

    return (
        <FieldWrapper fieldName={props.fieldName} fieldStyle={props.fieldStyle} description={props.description}>
            {renderComponent()}
        </FieldWrapper>
    )
};

ContactNumberField.propTypes = {
    fieldName: PropTypes.string,
    description: PropTypes.string,
};

export default ContactNumberField;