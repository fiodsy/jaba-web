import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import classes from '../TextField.css';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import noimage from '../../../assets/background/no_image.png';
import DeleteIcon from '@material-ui/icons/Delete';
import ImageUploadPreview from './ImageUploadPreview';
import FieldWrapper from '../FieldWrapper/FieldWrapper';

const options = [
    'Remove',
];

const ITEM_HEIGHT = 48;

const UploadBox = withStyles({
    root: {
        padding: '8px'
    },
})(Box);

const UploadWrapper = withStyles({
    root: {
        width: '100%',
        margin: '0 auto'

    },
})(Grid);


const ImageUploadAdvance = props => {
    const base64Snippet = 'data:image/png;base64,';
    var image = new Image();
    image.src = 'data:image/png;base64,iVBORw0K...';
    const [preview, setPreview] = useState({ imageFile: noimage, previewKey: null });
    const { imageFile, previewKey } = preview;

    const { addImageHandler, removeImageHandler, imageList } = props;
    const addImage = (base64Image) => {
        addImageHandler(base64Image);
    };

    const removeImage = (index) => {
        removeImageHandler(index);
    };
    //PROPERTY HANDLERS -- END

    //EVENT HANDLERS -- START
    const handleRemove = () => {
        removeImage(previewKey);
        if (imageList.length - 1 === 0) {
            setPreview({ imageFile: noimage, previewKey: null });
        } else if (previewKey === imageList.length - 1) {
            setPreview({ imageFile: base64Snippet + imageList[imageList.length - 2], previewKey: imageList.length - 2 });
        } else {
            setPreview({ imageFile: base64Snippet + imageList[imageList.length - 1], previewKey: imageList.length - 1 });
        }

    };

    const handleImageClick = (key) => {
        setPreview({ imageFile: base64Snippet + imageList[key], previewKey: key });
    }

    const uploadButtonEvent = (event) => {
        if ((props.maxImage === undefined || props.maxImage === null) ? true : imageList.length <= props.maxImage) {
            var file = event.target.files[0];
            const reader = new FileReader();
            var url = reader.readAsDataURL(file);

            reader.onload = function () {
                var base64data = reader.result;
                const chars = base64data.split(',');
                addImage(chars[1]);
                setPreview({ imageFile: base64Snippet + chars[1], previewKey: imageList.length - 1 });
            }

        }

    }
    //EVENT HANDLERS -- END
    const renderComponent = () => {
        return (
            <UploadWrapper
                className={classes.uploadWrapper}
                container
                direction="row"
                justify="center"
                alignItems="center"
                spacing={1}>
                <Grid item xs={12} >
                    <UploadBox className={classes.uploadBoxAdvance} component="div" color="red">

                        <div className={classes.menuUploadButton}>
                            {imageList.length === 0 ? null : <DeleteIcon color="action" onClick={handleRemove}></DeleteIcon>}
                        </div>

                        <div className={classes.uploadButton}>

                            <input
                                accept="image/*"
                                className={classes.imageinput}
                                id="contained-button-file"
                                multiple
                                type="file"
                                onChange={uploadButtonEvent}
                            />
                            <label htmlFor="contained-button-file">
                                <Button size="small" variant="contained" color="primary" component="span">
                                    Upload
                                    </Button>
                            </label>
                        </div>
                        <img id="image" className={classes.imgBox} src={imageList ? imageFile : noimage} />
                    </UploadBox>

                </Grid>

                <Grid container item xs={12} spacing={1}>
                    {imageList.map((imgPrev, index) => {
                        return (<ImageUploadPreview onClickHandler={handleImageClick} ikey={index} image={imgPrev}></ImageUploadPreview>
                        );
                    })}

                </Grid>
            </UploadWrapper>



        );

    }


    return (
        <FieldWrapper fieldName={props.fieldName} fieldStyle={props.fieldStyle} description={props.description}>
            {renderComponent()}
        </FieldWrapper>
    )
};

ImageUploadAdvance.propTypes = {
    fieldName: PropTypes.string,
    description: PropTypes.string,
    enableFieldWrapper: PropTypes.bool
};

export default ImageUploadAdvance;