import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Input from '@material-ui/core/Input';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import classes from '../TextField.css';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import * as Actions from '../../../redux/action';
import { useDispatch, useSelector } from 'react-redux';
import noimage from '../../../assets/background/no_image.png';
import DeleteIcon from '@material-ui/icons/Delete';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';



const options = [
    'Remove',

];



const ITEM_HEIGHT = 48;

const UploadBox = withStyles({
    root: {
        padding: '8px'
    },
})(Box);

const UploadWrapper = withStyles({
    root: {
        width: '100%',
        margin: '0 auto'

    },
})(Grid);



const UploadPreview = withStyles({
    root: {
        padding: '2px'
    },
})(Box);

const FieldName = withStyles({
    root: {
        paddingLeft: '15px'
    },
})(Grid);
const ImageUploadAdvance = props => {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };


    const dispatch = useDispatch();
    const base64Snippet = 'data:image/png;base64,';
    var image = new Image();
    image.src = 'data:image/png;base64,iVBORw0K...';

    const addImage = (base64Image) => dispatch(Actions.addImage(base64Image));
    const removeImage = (index) => dispatch(Actions.removeImage(index));
    const { currentIndex, images } = useSelector(state => ({
        currentIndex: state.imageUploadReducer.currentIndex,
        images: state.imageUploadReducer.images
    }));
    const [imagepreview, setImagePreview] = useState(noimage);
    const handleRemove = (event, num) => {
        alert(num);
        removeImage(num);
        handleClose
    };

    const uploadButtonEvent = (event) => {

        var file = event.target.files[0];
        const reader = new FileReader();
        var url = reader.readAsDataURL(file);

        reader.onload = function () {
            var base64data = reader.result;
            const chars = base64data.split(',');
            addImage(chars[1]);
           // setImagePreview(base64Snippet + images[0]);
        }
        reader.onloadend = function (e) {
            // get loaded data and render thumbnail.

        };


    }

    return (
        <Grid
            container
            direction="row"
            justify="center"
            alignItems="center"
            spacing={2}>
            <FieldName container item xs={4}>
                <Typography className={classes.fieldName} variant="h5" component="span">{props.fieldName}</Typography>
                <Typography className={classes.fieldDescription} variant="span" component="span">{props.description}</Typography>
            </FieldName>
            <Grid container item xs={8}>


                <UploadWrapper
                    className={classes.uploadWrapper}
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                    spacing={1}>
                    <Grid item xs={12} >
                        <UploadBox className={classes.uploadBoxAdvance} component="div" color="red">

                            <div className={classes.menuUploadButton}>
                                <IconButton
                                    aria-label="more"
                                    aria-controls="long-menu"
                                    aria-haspopup="true"
                                    onClick={handleClick}
                                >
                                    <MoreHorizIcon />
                                </IconButton>
                                <Menu
                                    id="long-menu"
                                    anchorEl={anchorEl}
                                    keepMounted
                                    open={open}
                                    onClose={handleClose}
                                    PaperProps={{
                                        style: {
                                            maxHeight: ITEM_HEIGHT * 4.5,
                                            width: '20ch',
                                        },
                                    }}
                                >
                                    {options.map((option) => (
                                        <MenuItem id="menuItem0" key={option} selected={option + "1" === 'Remove1'} onClick={(event) => handleRemove(event, 0)}>
                                            {option}
                                        </MenuItem>
                                    ))}
                                </Menu>
                            </div>

                            <div className={classes.uploadButton}>

                                <input
                                    accept="image/*"
                                    className={classes.imageinput}
                                    id="contained-button-file"
                                    multiple
                                    type="file"
                                    onChange={uploadButtonEvent}
                                />
                                <label htmlFor="contained-button-file">
                                    <Button size="small" variant="contained" color="primary" component="span">
                                        Upload
                                    </Button>
                                </label>


                            </div>


                            <img id="image" className={classes.imgBox} src={images[currentIndex - 1] ? base64Snippet + images[currentIndex - 1] : noimage} />



                        </UploadBox>

                    </Grid>

                    <Grid container item xs={12} spacing={1}>
                        <Grid className={classes.uploadPreviewWrapper} item xs={3}>
                            {images[currentIndex - 2] ?
                                <UploadPreview className={classes.uploadPreview} component="div" color="red">
                                    <div className={classes.menuUploadButton}>
                                        <IconButton
                                            aria-label="more"
                                            aria-controls="long-menu"
                                            aria-haspopup="true"
                                            onClick={handleClick}
                                        >
                                            <MoreHorizIcon />
                                        </IconButton>
                                        <Menu
                                            id="long-menu"
                                            anchorEl={anchorEl}
                                            keepMounted
                                            open={open}
                                            onClose={handleClose}
                                            PaperProps={{
                                                style: {
                                                    maxHeight: ITEM_HEIGHT * 4.5,
                                                    width: '20ch',
                                                },
                                            }}
                                        >
                                            {options.map((option) => (
                                                <MenuItem id="menuItem1" key={option} selected={option === 'Remove'} onClick={(event) => handleRemove(event, 1)}>
                                                    {option}
                                                </MenuItem>
                                            ))}
                                        </Menu>
                                    </div>
                                    {/* <div className={classes.uploadButton}>
                                    <input
                                        accept="image/*"
                                        className={classes.imageinput}
                                        id="contained-button-file"
                                        multiple
                                        type="file"
                                        onChange={uploadButtonEvent}
                                    />
                                    <label htmlFor="contained-button-file">
                                        <Button size="small" variant="contained" color="primary" component="span">
                                            Upload
                                    </Button>
                                    </label>
                                </div> */}

                                    <img id="image" className={classes.imgBox} src={images[currentIndex - 2] ? base64Snippet + images[currentIndex - 2] : noimage} />

                                </UploadPreview>
                                : null}

                        </Grid>
                        <Grid className={classes.uploadPreviewWrapper} item xs={3}>
                            {images[currentIndex - 3] ?

                                <UploadPreview className={classes.uploadPreview} component="div" color="red">
                                    <div className={classes.menuUploadButton}>
                                        <IconButton
                                            aria-label="more"
                                            aria-controls="long-menu"
                                            aria-haspopup="true"
                                            onClick={handleClick}
                                        >
                                            <MoreHorizIcon />
                                        </IconButton>
                                        <Menu
                                            id="long-menu"
                                            anchorEl={anchorEl}
                                            keepMounted
                                            open={open}
                                            onClose={handleClose}
                                            PaperProps={{
                                                style: {
                                                    maxHeight: ITEM_HEIGHT * 4.5,
                                                    width: '20ch',
                                                },
                                            }}
                                        >
                                            {options.map((option) => (
                                                <MenuItem id="menuItem2"  key={option} selected={option + "3" === 'Remove'} onClick={(event) => handleRemove(event, 2)}>
                                                    {option}
                                                </MenuItem>
                                            ))}
                                        </Menu>
                                    </div>
                                    {/* <div className={classes.uploadButton}>
                                    <input
                                        accept="image/*"
                                        className={classes.imageinput}
                                        id="contained-button-file"
                                        multiple
                                        type="file"
                                        onChange={uploadButtonEvent}
                                    />
                                    <label htmlFor="contained-button-file">
                                        <Button size="small" variant="contained" color="primary" component="span">
                                            Upload
                                    </Button>
                                    </label>
                                </div> */}
                                    <img id="image" className={classes.imgBox} src={images[currentIndex - 3] ? base64Snippet + images[currentIndex - 3] : noimage} />
                                </UploadPreview>
                                : null}
                        </Grid>
                        <Grid className={classes.uploadPreviewWrapper} item xs={3}>
                            {images[currentIndex - 4] ?
                                <UploadPreview className={classes.uploadPreview} component="div" color="red">
                                    <div className={classes.menuUploadButton}>
                                        <IconButton
                                            aria-label="more"
                                            aria-controls="long-menu"
                                            aria-haspopup="true"
                                            onClick={handleClick}
                                        >
                                            <MoreHorizIcon />
                                        </IconButton>
                                        <Menu
                                            id="long-menu"
                                            anchorEl={anchorEl}
                                            keepMounted
                                            open={open}
                                            onClose={handleClose}
                                            PaperProps={{
                                                style: {
                                                    maxHeight: ITEM_HEIGHT * 4.5,
                                                    width: '20ch',
                                                },
                                            }}
                                        >
                                            {options.map((option) => (
                                                <MenuItem id="menuItem3"  key={option} selected={option === 'Remove'} onClick={(event) => handleRemove(event, 3)}>
                                                    {option}
                                                </MenuItem>
                                            ))}
                                        </Menu>
                                    </div>
                                    {/* <div className={classes.uploadButton}>
                                    <input
                                        accept="image/*"
                                        className={classes.imageinput}
                                        id="contained-button-file"
                                        multiple
                                        type="file"
                                        onChange={uploadButtonEvent}
                                    />
                                    <label htmlFor="contained-button-file">
                                        <Button size="small" variant="contained" color="primary" component="span">
                                            Upload
                                    </Button>
                                    </label>
                                </div> */}
                                    <img id="image" className={classes.imgBox} src={images[currentIndex - 4] ? base64Snippet + images[currentIndex - 4] : noimage} />
                                </UploadPreview>
                                : null}

                        </Grid>
                        <Grid className={classes.uploadPreviewWrapper} item xs={3}>
                            {images[currentIndex - 5] ?
                                <UploadPreview className={classes.uploadPreview} component="div" color="red">
                                    <div className={classes.menuUploadButton}>
                                        <IconButton
                                            aria-label="more"
                                            aria-controls="long-menu"
                                            aria-haspopup="true"
                                            onClick={handleClick}
                                        >
                                            <MoreHorizIcon />
                                        </IconButton>
                                        <Menu
                                            id="long-menu"
                                            anchorEl={anchorEl}
                                            keepMounted
                                            open={open}
                                            onClose={handleClose}
                                            PaperProps={{
                                                style: {
                                                    maxHeight: ITEM_HEIGHT * 4.5,
                                                    width: '20ch',
                                                },
                                            }}
                                        >
                                            {options.map((option) => (
                                                <MenuItem id="menuItem4"  key={option} selected={option === 'Remove'} onClick={(event) => handleRemove(event, 4)}>
                                                    {option}
                                                </MenuItem>
                                            ))}
                                        </Menu>
                                    </div>
                                    {/* <div className={classes.uploadButton}>
                                    <input
                                        accept="image/*"
                                        className={classes.imageinput}
                                        id="contained-button-file"
                                        multiple
                                        type="file"
                                        onChange={uploadButtonEvent}
                                    />
                                    <label htmlFor="contained-button-file">
                                        <Button size="small" variant="contained" color="primary" component="span">
                                            Upload
                                    </Button>
                                    </label>
                                </div> */}
                                    <img id="image" className={classes.imgBox} src={images[currentIndex - 5] ? base64Snippet + images[currentIndex - 5] : noimage} />
                                </UploadPreview>
                                : null}
                        </Grid>

                        <Typography className={classes.fieldDescription} variant="span" component="span">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
    XXX px x XXX px</Typography>
                    </Grid>




                </UploadWrapper>



            </Grid>
        </Grid>
    );
};

ImageUploadAdvance.propTypes = {
    fieldName: PropTypes.string,
    description: PropTypes.string,
};

export default ImageUploadAdvance;