import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { withStyles } from '@material-ui/core/styles';
import classes from '../TextField.css';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../../redux/action';
import noimage from '../../../assets/background/no_image.png';
const options = [
    'Remove',

];
const UploadPreview = withStyles({
    root: {
        padding: '2px'
    },
})(Box);

const ITEM_HEIGHT = 48;
const ImageUploadPreview = props => {
    const base64Snippet = 'data:image/png;base64,';
    const dispatch = useDispatch();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const removeImage = (index) => dispatch(Actions.removeImage(index));
    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleRemove = (event, num) => {
        alert(num);
        removeImage(num);
        handleClose
    };

    const clickPreviewEvent = (onClickHandler, key) => {
        onClickHandler(key);
       // setDefaultSize(props.fieldSize - event.target.value.length);
    }

    return (
        <Grid  className={classes.uploadPreviewWrapper} item xs={3}>
        {props.image ?
            <UploadPreview onClick={(event) => clickPreviewEvent(props.onClickHandler, props.ikey)} className={classes.uploadPreview} component="div" color="red">
                <img id="image" key={props.ikey} className={classes.imgBox} src={props.image ? base64Snippet + props.image : noimage} />
            </UploadPreview>
            : null}

    </Grid>
    );
};

ImageUploadPreview.propTypes = {
    
};

export default ImageUploadPreview;