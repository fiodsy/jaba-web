import Chip from '@material-ui/core/Chip';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import React, { useState } from 'react';
import FieldWrapper from '../FieldWrapper/FieldWrapper';


const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        justifyContent: 'center',
        flexWrap: 'wrap',
        listStyle: 'none',
        padding: theme.spacing(0.5),
        margin: 0,
    },
    chip: {
        margin: theme.spacing(0.5),
    },
}));
const randomString = (length) => {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}
const ChipArrayField = (props) => {
    const { addHandler, removeHandler, dataHandler, ...other } = props;

    const [value, setValue] = useState();
    const classes = useStyles();
    const [chipData, setChipData] = React.useState(dataHandler);
    const handleDelete = (chipToDelete) => () => {
        removeHandler(chipToDelete);
    };

    const keyPress = (event) => {
        if (dataHandler.length !== 20) {
            addHandler(event.target.value);
            event.target.value = "";
        } else {
            alert("Only 20 Hashtag");
        }
    }

    const renderComponent = () => {

        return (<div><TextField
            value={value}
            name={props.name}
            id="outlined-full-width"
            placeholder="Placeholder"
            fullWidth
            InputLabelProps={{
                shrink: true,
            }}
            variant="outlined"
            onKeyPress={(event) => {
                if (event.key === "Enter") {
                    keyPress(event, props.onChange);

                }
            }}

        />
            <Paper component="ul" className={classes.root}>
                {dataHandler.map((data, index) => {
                    let icon;


                    return (
                        <li key={index}>
                            <Chip
                                icon={icon}
                                label={data}
                                onDelete={handleDelete(index)}
                                className={classes.chip}
                            />
                        </li>
                    );
                })}
            </Paper></div>);
    }

  

    return (
        <FieldWrapper fieldName={props.fieldName} fieldStyle={props.fieldStyle} description={props.description}>
            {renderComponent()}
        </FieldWrapper>
    )
}

ChipArrayField.propTypes = {

}

export default ChipArrayField

