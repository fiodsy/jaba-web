import React, { useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Input from '@material-ui/core/Input';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import classes from '../TextField.css';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import FormGroup from '@material-ui/core/FormGroup';
import FolderIcon from '@material-ui/icons/Folder';
import DeleteIcon from '@material-ui/icons/Delete';
import MenuItem from '@material-ui/core/MenuItem';
import { DropzoneArea } from 'material-ui-dropzone';
import { useDropzone } from 'react-dropzone';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../../redux/action';
import FieldWrapper from '../FieldWrapper/FieldWrapper';
const docTypes = [
    {
        value: 'PRC',
        label: 'Professional License',
    },
    {
        value: 'PASSPORT',
        label: 'Passport',
    },
    {
        value: 'SSS',
        label: 'Social Security Service ID',
    },
    {
        value: 'LTO',
        label: 'Driver\'s License',
    },
    {
        value: 'DOCUMENT_WITH_SELFIE',
        label: 'Document',
    },
    {
        value: 'SENIOR_CITIZEN_ID',
        label: 'Senior Citizen ID',
    }, {
        value: 'UMID',
        label: 'UMID',
    }
    
];



const DocumentField = props => {
    const dispatch = useDispatch();
    const incrementIndex = () => dispatch(Actions.incrementIndex());
    const [fileLimit, setDocumentLimit] = useState(3);
    const addDocument = (base64Image, value, key) => dispatch(Actions.updateDocument({ base64Image, value, key }));
    const removeDocument = (index) => dispatch(Actions.removeDocument(index));
    const setDocumentType = (key, value, index) => dispatch(Actions.updateDocumentFieldByIndex(key, value, index));
    const { documents } = useSelector(state => ({
        documents: state.documentUploadReducer.documentResponse,
    }));



    const handleChange = (event, index) => {
        setDocumentType("documentType", event.target.value, index);
    };

    const handleRemove = (event, index) => {
        removeDocument(index);
    };

    const dropFiles = (files) => {
        files.map((file, index) => {
            if (index === files.length - 1) {
                const reader = new FileReader();
                var url = reader.readAsDataURL(file);
                reader.onloadend = function () {
                    const base64data = reader.result;
                    const chars = base64data.split(',');
                    addDocument(chars[1], file.name, index);
                }

            }
        })

    };

    const [secondary, setSecondary] = React.useState(false);
    const [dense, setDense] = React.useState(false);



    const renderComponent=()=>{
        return(<Box className={classes.fileUploadBox} component="div" color="red">
        <Grid
            container
            direction="row"
            justify="center"
            alignItems="center"
        >
            <Grid item xs={12}>
                <DropzoneArea
                    dropzoneClass={classes.dropZoneArea}
                    showPreviewsInDropzone={false}
                    onChange={(files) => dropFiles(files)}
                />
    
            </Grid>
            <Grid item xs={12}>
                <List dense={dense}>
                    {documents ? documents.map((fileData, index) =>
                        <ListItem>
                            <ListItemAvatar>
                                <Avatar>
                                    <FolderIcon />
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText
                                primary={fileData.value}
                            />
    
                            <ListItemText
                                primary={<TextField
                                    id="select-type"
                                    select
                                    value={fileData.documentType}
                                    onChange={(event) => handleChange(event, index)}
                                    margin="dense"
                                    size="small"
                                    variant="outlined"
                                    fullWidth>
                                    {docTypes.map((option) => (
                                        <MenuItem key={option.value} value={option.value}>
                                            {option.label}
                                        </MenuItem>
                                    ))}
                                </TextField>} />
    
                            <ListItemSecondaryAction>
                                <IconButton edge="end" aria-label="delete">
                                    <DeleteIcon onClick={(event) => handleRemove(event, index)}/>
                                </IconButton>
                            </ListItemSecondaryAction>
                        </ListItem>,
                    ) : <span>No Upload</span>}
                </List>
            </Grid>
        </Grid>
    </Box>);
    }

 
    return (
        <FieldWrapper fieldName={props.fieldName} fieldStyle={props.fieldStyle} description={props.description}>
            {renderComponent()}
        </FieldWrapper>
    )
};

DocumentField.propTypes = {

};

export default DocumentField;