import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Input from '@material-ui/core/Input';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import classes from '../TextField.css';
import FieldWrapper from '../FieldWrapper/FieldWrapper';

const FieldName = withStyles({
    root: {
        paddingLeft: '15px'
    },
})(Grid);
const JabaTextField = props => {

    const [defaultSize, setDefaultSize] = useState(props.fieldSize);
    const [isDisable, setDisable] = useState(props.edit);
    const disableHandler = (value) =>{
        setDisable(value);
    }
    const fieldSizeHandler = (size) => {
        return defaultSize + "/" + size + " characters"
    }

    const textFieldOnChange = (onChangeValue, event) => {
        onChangeValue(event);
        setDefaultSize(props.fieldSize - event.target.value.length);
    }


    const renderComponent = () => {
    
        return (<TextField  disabled={isDisable}
            name={props.name}
            id="outlined-full-width"
            placeholder="Placeholder"
            fullWidth
            InputLabelProps={{
                shrink: true,
            }}
            value={props.value}
            inputProps={{
                maxLength: props.fieldSize !== undefined ? props.fieldSize : 25,
            }}
            variant="outlined"
            onChange={(event) => textFieldOnChange(props.onChange, event)} />);
    }

    return (
        <FieldWrapper disableHandler={disableHandler} name={props.name} updateHandler={props.updateHandler} editHandler={props.editHandler} isEditField={props.edit} fieldSizeHandler={fieldSizeHandler(props.fieldSize)} fieldSize={props.fieldSize} fieldName={props.fieldName} fieldStyle={props.fieldStyle} description={props.description}>
            {renderComponent()}
        </FieldWrapper>
    )
};

JabaTextField.propTypes = {
    name: PropTypes.string,
    fieldName: PropTypes.string,
    description: PropTypes.string,
    fieldSize: PropTypes.number
};

export default JabaTextField;