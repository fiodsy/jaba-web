import React, { useState } from 'react';
import PropTypes from 'prop-types';
import NumberFormat from 'react-number-format';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import { useDispatch, useSelector } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import classes from '../TextField.css';
import Typography from '@material-ui/core/Typography';
import FieldWrapper from '../FieldWrapper/FieldWrapper';

const NumberFormatCustom = (props) => {
    const { inputRef, onChange, ...other } = props;

    return (
        <NumberFormat
            {...other}
            getInputRef={inputRef}
            onValueChange={(values) => {
                onChange({
                    target: {
                        name: props.name,
                        value: values.value,
                    }
                });
            }}
            thousandSeparator
            isNumericString
            prefix="₱"
        />
    )
}
const AmountField = (props) => {
    const [value, setValue] = useState();

    const renderComponent = () => {
        return (<TextField
            name={props.name}
            id={props.id}
            onChange={props.onChange}
            value={value}
            fullWidth
            InputLabelProps={{
                shrink: true,
            }}
            InputProps={{
                inputComponent: NumberFormatCustom,
            }}

            variant="outlined" />);

    }

    return (
        <FieldWrapper fieldName={props.fieldName} fieldStyle={props.fieldStyle} description={props.description}>
            {renderComponent()}
        </FieldWrapper>
    )
}

AmountField.propTypes = {

}

export default AmountField

