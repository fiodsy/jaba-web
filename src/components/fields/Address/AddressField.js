import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Input from '@material-ui/core/Input';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import classes from '../TextField.css';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import * as ApiService from '../../../api/ApiService';
import { useAuth } from '../../context/auth';
import * as Actions from '../../../redux/action';
import { useDispatch, useSelector } from 'react-redux';
import FieldWrapper from '../FieldWrapper/FieldWrapper';
const FieldName = withStyles({
    root: {
        paddingLeft: '15px'
    },
})(Grid);

const AddressField = (props) => {
    const dispatch = useDispatch();

    const { provinces, municipalities, barangays, fieldDisable } = useSelector(state => ({
        provinces: state.addressReducer.dataList.provinces,
        municipalities: state.addressReducer.dataList.municipalities,
        barangays: state.addressReducer.dataList.barangays,
        fieldDisable: state.addressReducer.disableState
    }));

    const switchDisable = (name, value) => dispatch(Actions.fieldDisableHandler(name, value));

    const addressData = useSelector(state => state.addressReducer.address);

    const { authTokens } = useAuth();



    const eventHandler = (event) => {
        const value = event.target.value;
        dispatch(Actions.updateAddress([event.target.name], value));
    }

    const provinceEventHandler = (event) => {
        dispatch(Actions.listMunicipals(authTokens, event.target.value));
        switchDisable("municipality", !fieldDisable.municipality);
        eventHandler(event);
    }

    const municipalEventHandler = (event) => {
        dispatch(Actions.listBarangays(authTokens, event.target.value));
        switchDisable("barangay", !fieldDisable.barangay);
        eventHandler(event);
    }

    const barangayEventHandler = (event) => {
        switchDisable("zipcode", !fieldDisable.zipcode);
        eventHandler(event);
    }

    const zipcodeEventHandler = (event) => {
        switchDisable("streetBldgFlrUnit", !fieldDisable.streetBldgFlrUnit);
        eventHandler(event);
    }

    const streetBldgFlrUnitEventHandler = (event) => {
        eventHandler(event);
    }

    const renderProvince = () => {
        return (<Select
            style={{
                marginTop: 8,
                marginBottm: 8
            }}
            labelId="demo-simple-select-outlined-label"
            id="demo-simple-select-outlined"
            value={addressData.province}
            onChange={provinceEventHandler}
            fullWidth
            variant="outlined"
            name="province"
            disabled={fieldDisable.province}
        >
            <MenuItem value="">
                <em>None</em>
            </MenuItem>
            {provinces.map(prov => (<MenuItem value={prov.code}>{prov.description}</MenuItem>))}
        </Select>);
    }



    const renderMunicipal = () => {
        return (<Select
            style={{
                marginTop: 8,
                marginBottm: 8
            }}
            labelId="demo-simple-select-outlined-label"
            id="demo-simple-select-outlined"
            value={addressData.municipality}
            onChange={municipalEventHandler}
            fullWidth
            margin="normal"
            variant="outlined"
            name="municipality"
            disabled={fieldDisable.municipality}
        >

            {municipalities ? municipalities.map(mun => (<MenuItem value={mun.code}>{mun.description}</MenuItem>)) : <MenuItem value="">
                <em>None</em>
            </MenuItem>}
        </Select>);
    }



    const renderBarangay = () => {
        return (<Select
            style={{
                marginTop: 8,
                marginBottm: 8
            }}
            labelId="demo-simple-select-outlined-label"
            id="demo-simple-select-outlined"
            value={addressData.barangay}
            onChange={barangayEventHandler}
            fullWidth
            variant="outlined"
            name="barangay"
            disabled={fieldDisable.barangay}
        >
            {barangays ? barangays.map(brgy => (<MenuItem value={brgy.code}>{brgy.description}</MenuItem>)) : <MenuItem value="">
                <em>None</em>  </MenuItem>}
        </Select>);
    }


    const renderZipcode = () => {
        return (<TextField
            onChange={zipcodeEventHandler}
            id="field-zipcode"
            style={{
                marginTop: 8,
                marginBottm: 8
            }}
            placeholder="Placeholder"
            fullWidth
            InputLabelProps={{
                shrink: true,
            }}
            Input
            name="zipcode"
            value={addressData.zipcode}
            variant="outlined"
            disabled={fieldDisable.zipcode} />);
    }


    const renderMiscAdd = () => {
        return (<TextField
            onChange={streetBldgFlrUnitEventHandler}
            id="field-streetbldg"
            style={{
                marginTop: 8,
                marginBottm: 8
            }}
            placeholder="Placeholder"
            multiline
            rows={3}
            fullWidth
            InputLabelProps={{
                shrink: true,
            }}
            variant="outlined"
            disabled={fieldDisable.streetBldgFlrUnit}
            name="streetBldgFlrUnit"
            value={addressData.streetBldgFlrUnit}
        />);
    }

    return provinces ? (
        <div>
            <FieldWrapper fieldStyle={props.fieldStyle} fieldName="Province" fieldStyle={props.fieldStyle} description={props.description}>
                {renderProvince()}
            </FieldWrapper>

            <FieldWrapper fieldStyle={props.fieldStyle} fieldName="City/Municipality" fieldStyle={props.fieldStyle} description={props.description}>
                {renderMunicipal()}
            </FieldWrapper>
        
            <FieldWrapper fieldStyle={props.fieldStyle} fieldName="Barangay" fieldStyle={props.fieldStyle} description={props.description}>
                {renderBarangay()}
            </FieldWrapper>

            <FieldWrapper fieldStyle={props.fieldStyle} fieldName="Zip Code" fieldStyle={props.fieldStyle} description={props.description}>
                {renderZipcode()}
            </FieldWrapper>

            <FieldWrapper fieldStyle={props.fieldStyle} fieldName="Street/Building/Floor Unit" fieldStyle={props.fieldStyle} description={props.description}>
                {renderMiscAdd()}
            </FieldWrapper>

        </div>

    ) : (
        <div>Loading...</div>
    );


};

AddressField.propTypes = {

};



export default AddressField;