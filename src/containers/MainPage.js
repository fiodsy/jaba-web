
import React, { Component } from 'react';
import MainPageStage from '../components/layout/stage/MainPageStage';



const sections = [
  { title: 'Home', url: '/dashboard' },
  { title: 'Organization', url: '/login' },
  { title: 'Product', url: '/product' },
  { title: 'Item', url: '/item' },
  { title: 'Negotiation', url: '/product-list' },
  { title: 'Booking', url: '/booking' },
  { title: 'Transaction', url: 'transaction' }
];

class MainPage extends Component {


  state = {
    open: false
  }


  render() {

    const handleOpen = () => {
      this.setState({ open: true })
    };

    const handleClose = () => {
      this.setState({ open: false })
    };
    return (
      <MainPageStage sections={sections}></MainPageStage>
    );
  }
}

export default MainPage;
