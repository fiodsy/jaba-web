import React, { Component } from 'react';
import RegistrationStage from '../components/layout/stage/RegistrationStage';
import * as ApiService from '../api/ApiService';
import { BrowserRouter, Route, withRouter } from 'react-router-dom';
import LoginStage from '../components/layout/stage/LoginStage';


class Registration extends Component {

    state = ({
        registration: {
            email: null,
            password: null
        }
    });

    submitRegistration = (event) => {
        event.preventDefault();
        const request = {
            email: this.state.registration.email,
            password: this.state.registration.password
        };
        ApiService.register(request).then(async response => {
            this.props.history.push('/login');
        });
    }

    emailChangeHandler = (event) => {
        var registration = this.state.registration;
        registration.email = event.target.value;
        this.setState({ registration: registration });

    }

    passwordChangeHandler = (event) => {
        var registration = this.state.registration;
        registration.password = event.target.value;
        this.setState({ registration: registration });
    }


    render() {
        return (
            <div>
                <RegistrationStage passwordChange={this.passwordChangeHandler} emailChange={this.emailChangeHandler} submit={this.submitRegistration}>
                </RegistrationStage>
            </div>
        );
    }
}

export default Registration;