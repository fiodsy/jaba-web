import React, { useState } from 'react';
import MainPageStage from '../components/layout/stage/MainPageStage';
import RegistrationStage from '../components/layout/stage/RegistrationStage';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { AuthContext } from "../components/context/auth";
import classes from '../containers/App.css';
import LoginStage from '../components/layout/stage/LoginStage';
import { createStore, applyMiddleware, compose } from 'redux';
import reducers from '../redux/reducer';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import * as Actions from '../redux/action';
import * as url from '../api/backendserver';
import { composeWithDevTools } from 'redux-devtools-extension';



function App(props) {
  const existingTokens = JSON.parse(localStorage.getItem("token"));
  const existingUserId = JSON.parse(localStorage.getItem("user"));
  const [authTokens, setAuthTokens] = useState(existingTokens);
  const [userId, setUserId] = useState(existingUserId);

  const setTokens = (data) => {
    localStorage.setItem("token", JSON.stringify(data));
    setAuthTokens(data);
  }

  const setUser = (data) => {
    localStorage.setItem("user", JSON.stringify(data));
    setUserId(data);
  }

  const composeEnhancers = composeWithDevTools({
    trace: true,
    traceLimit: 25
  });
  //const composeEnhancers = composeWithDevTools({ actionCreators, trace: true, traceLimit: 25 });

  const enhancer = composeEnhancers(
    applyMiddleware(thunk),

    // other store enhancers if any
  );
  const store = createStore(reducers, enhancer);
  store.dispatch(Actions.listProvinces(authTokens));
  store.dispatch(Actions.fetchItemTypeLookUp(authTokens, url.ITEM_TYPE));

  //const store = createStore(reducers, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(), applyMiddleware(thunk));
  return (<div className={classes.wrapper}>
    <Provider store={store}>
      <AuthContext.Provider value={{ userId, setUserId: setUser, authTokens, setAuthTokens: setTokens }} >
        <BrowserRouter>

          <Switch>
            <Route path="/" exact component={RegistrationStage} />
            <Route path="/login" component={LoginStage} />
            <Route exact path="/dashboard" component={MainPageStage} />
            <Route exact path="/organization" component={MainPageStage} />
            <Route exact path="/product" component={MainPageStage} />
            <Route exact path="/item" component={MainPageStage} />
            <Route exact path="/product-list" component={MainPageStage} />
          </Switch>
        </BrowserRouter>
      </AuthContext.Provider>
    </Provider>
  </div>);
}


export default App;
