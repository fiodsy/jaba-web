import * as Type from '../action/types';


const INITIAL_STATE = {
    address: {
        barangay: '',
        country: 'PHILIPPINES',
        municipality: '',
        province: '',
        streetBldgFlrUnit: '',
        zipcode: '',
    },
    errorMessage: '',
    dataList: {
        provinces: null,
        municipalities: null,
        barangays: null
    }, disableState: {
        province: false,
        municipality: true,
        barangay: true,
        streetBldgFlrUnit: true,
        zipcode: true
    },
};

export default (state = INITIAL_STATE, action) => {

    switch (action.type) {
        case Type.CHANGE_DISABLE_STATE: {
            return { ...state, disableState: { ...state.disableState, [action.payload.prop]: action.payload.value } }
        }
        case Type.UPDATE_ADDRESS: {
            return { ...state, address: { ...state.address, [action.payload.prop]: action.payload.value } }
        }
        case Type.GET_PROVINCE_LIST: {
            return {
                ...state, dataList: { provinces: action.payload }
            }

        }
        case Type.GET_MUNICIPAL_LIST: {
            return {
                ...state, dataList: { ...state.dataList, municipalities: action.payload }
            }

        }
        case Type.GET_BARANGAY_LIST: {
            return {
                ...state, dataList: { ...state.dataList, barangays: action.payload }
            }

        }
        default: return state
    }
}