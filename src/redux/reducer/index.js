import { combineReducers } from 'redux';
import AddressFieldReducer from './AddressFieldReducer';
import JabaTextFieldReducer from './JabaTextFieldReducer';
import ContactNumberReducer from './ContactNumberReducer';
import DocumentUploadReducer from './DocumentUploadReducer';
import LookUpReducer from './LookUpReducer';
import ItemReducer from './ItemReducer';
import ImageUploadAdvanceReducer from './ImageUploadAdvanceReducer';
import ProductReducer from './ProductReducer';
export default combineReducers({
    addressReducer: AddressFieldReducer,
    jabaTextFieldReducer: JabaTextFieldReducer,
    contactNumberReducer: ContactNumberReducer,
    documentUploadReducer: DocumentUploadReducer,
    lookUpReducer: LookUpReducer,
    itemReducer: ItemReducer,
    imageUploadReducer: ImageUploadAdvanceReducer,
    productReducer: ProductReducer
})