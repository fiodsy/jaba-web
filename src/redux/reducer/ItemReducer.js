
import * as Type from '../action/types';

const INITIAL_STATE = {
    data: {
        id: '',
        itemCode: '',
        base64Images: [],
        categoryId: '',
        description: '',
        hashtags: [],
        merchantId: '',
        name: '',
        price: ''
    },
    errorMessage: '',
    itemData: {
        count: null,
        items: []
    },
    filter: {
        filterBy: {
            matches: [
                {
                    key: 'isActive',
                    value: 'TRUE'
                }
            ]
        },
        pagination: {
            from: 0,
            size: 10
        },
        sortBy: {
            fieldsAscOrDesc: {}
        }
    }
};

export default (state = INITIAL_STATE, action) => {

    switch (action.type) {
        case Type.ITEM_FIELD_CHANGE: {
            return { ...state, data: { ...state.data, [action.payload.prop]: action.payload.value } }
        }
        case Type.ADD_HASHTAG: {
            return { ...state, data: { ...state.data, hashtags: [...state.data.hashtags, action.payload.value] } }
        }
        case Type.REMOVE_HASHTAG: {
            return { ...state, data: { ...state.data, hashtags: [...state.data.hashtags.filter((item, index) => index !== action.payload.index)] } }
        } 
        case Type.ADD_ITEM_IMAGE: {
            return { ...state, data: { ...state.data, base64Images: [...state.data.base64Images, action.payload.value] }}
        }

        case Type.REMOVE_ITEM_IMAGE: {
            return { ...state, data: { ...state.data, base64Images: [...state.data.base64Images.filter((item, index) => index !== action.payload.index)] }}
        }

        case Type.FETCH_ALL_ACTIVE_ITEM: {
            return { ...state, itemData: { items: action.payload.items, count: action.payload.totalHits } }
        } 
        case Type.FILTER_ITEM_MATCHES: {
            return {
                ...state, filter: {
                    ...state.filter,
                    filterBy: {
                        ...state.filter.filterBy,
                        matches: [...state.filter.filterBy.matches,
                        { key: action.payload.key, value: action.payload.value }]
                    }
                }
            }
        } case Type.FILTER_ITEM_MATCHES_LIKE: {
            return {
                ...state, filter: {
                    ...state.filter,
                    filterBy: {
                        ...state.filter.filterBy,
                        matches: setMatchParam(state.filter.filterBy.matches,
                            action.payload.key, action.payload.value)
                    }
                }
            }
        }

        case Type.FILTER_ITEM_SET_SIZE: {
            return {
                ...state, filter: {
                    ...state.filter,
                    pagination: {
                        ...state.filter.pagination,
                        size: action.payload
                    }
                }
            }
        } case Type.FILTER_ITEM_SET_PAGE: {
            return {
                ...state, filter: {
                    ...state.filter,
                    pagination: {
                        ...state.filter.pagination,
                        from: action.payload
                    }
                }
            }
        } case Type.INITIALIZE_ITEM: {
            return undefined;
        }

        default: return state
    }
}

const setMatchParam = (existingParams, key, value) => {
    const existingItem = [...existingParams.filter((item) => item.key !== key)];
    const newItem = [...existingItem.map((item) => {
        if (item.key === key) {
            return { ...item, value: value }
        } else {
            return ({ key: key, value: value });
        }   
    })];
 
  
    return [...newItem, ...existingItem];
}