import * as Type from '../action/types';


const INITIAL_STATE = {
    fieldProps: [
        {
            key: '1',
            id: '1',
            index: 0,
        },
    ],
    contactInformations: [
        { key: 'mobilenumber' }
    ],
    currentIndex: 1,
    errorMessage: '',
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case Type.UPDATE_FIELD_PROPS: {
            return { ...state, fieldProps: [...state.fieldProps, action.payload.value] }
        }
        case Type.INCREMENT_INDEX: {
            return { ...state, currentIndex: state.currentIndex + 1 }
        }
        case Type.UPDATE_CONTACT_INFORMATION: {
            return { ...state, contactInformations: [...state.contactInformations, { [action.payload.prop]: action.payload.value }] }
        } case Type.UPDATE_CONTACT_INFORMATION_BY_INDEX: {

            return {
                ...state, contactInformations: [...state.contactInformations.map((item, index) => {
                    if (index === action.payload.index) {
                        return { ...item, [action.payload.prop]: "+".concat(action.payload.value) }
                    }

                    return item;
                })]
            }

        }
        case Type.GET_CONTACT_INFORMATION_BY_INDEX: {
            state.contactInformations.map((item, index) => {
                if (index === action.payload.index) {
                    alert(JSON.stringify(item));
                    return item;
                }
            })

        }
    }
    return state;
}
