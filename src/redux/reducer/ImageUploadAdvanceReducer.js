
import * as Type from '../action/types';


const INITIAL_STATE = {
    images: [],
    currentIndex: 0,
    errorMessage: '',
    key: null,
};

export default (state = INITIAL_STATE, action) => {

    switch (action.type) {
        case Type.ADD_IMAGE: {
            return { ...state, currentIndex: state.currentIndex + 1, images: [...state.images, action.payload.value] }
        }
        case Type.REMOVE_IMAGE: {
            return {
                ...state,currentIndex: state.currentIndex - 1,
                images: [...state.images.slice(0, action.payload.index), ...state.images.slice(action.payload.index + 1)]
            }

            // let newArray = state.images.slice()
            // newArray.splice(action.payload.index, 1)
            // return newArray
            // return { ...state, documentResponse: [...state.documentResponse, action.payload.value] }
           // return { ...state, images: [...state.images.filter((item, index) => index !== action.payload.index)] }
        }

        case Type.UPDATE_IMAGE_BY_INDEX: {
            return {
                ...state, images: [...state.images.map((item, index) => {
                    if (index === action.payload.index) {
                        return { ...item, [action.payload.prop]: action.payload.value }
                    }

                    return item;
                })]
            }

        }
        case Type.GET_IMAGE_BY_INDEX: {
            state.images.map((item, index) => {
                if (index === action.payload.index) {
                    return item;
                }
            })

        }
        default: return state
    }

    return state;

}