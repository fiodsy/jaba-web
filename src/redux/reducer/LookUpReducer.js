import * as Type from '../action/types';

const INITIAL_STATE = {
    itemTypes: [{
    }],
    productCategories: [{
    }],
};

export default (state = INITIAL_STATE, action) => {

    switch (action.type) {


        case Type.FETCH_PRODUCT_CATEGORY: {
            return {
                ...state, productCategories: [ ...action.payload ]
            }

        }
        case Type.FETCH_ITEM_TYPE: {
            return {
                ...state, itemTypes: [...action.payload]
            }

        }

        default: return state
    }
}