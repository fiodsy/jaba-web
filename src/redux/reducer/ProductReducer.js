
import * as Type from '../action/types';

const DEFAULT_STATE = {
    data: {
        active: true,
        addressTag: [],
        base64CoverImage: '',
        base64Images: [],
        categoryCode: null,
        description: null,
        hashtags: [],
        itemIds: [],
        merchantId: null,
        name: null,
        negotiable: true,
        price: 0
    },
    productData: {
        count: null,
        products: []
    },
    errorMessage: '',

    filter: {
        filterBy: {
            matches: [
             
            ]
        },
        pagination: {
            from: 0,
            size: 10
        },
        sortBy: {
            fieldsAscOrDesc: {}
        },
        withoutImage: true
    }
};


const INITIAL_STATE = {
    data: {
        active: true,
        addressTag: [],
        base64CoverImage: '',
        base64Images: [],
        categoryCode: null,
        description: null,
        hashtags: [],
        itemIds: [],
        merchantId: null,
        name: null,
        negotiable: true,
        price: 0
    },
    productData: {
        count: null,
        products: []
    },
    errorMessage: '',

    filter: {
        filterBy: {
            matches: [
             
            ]
        },
        pagination: {
            from: 0,
            size: 10
        },
        sortBy: {
            fieldsAscOrDesc: {}
        },
        withoutImage: true
    }
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case Type.FETCH_ALL_ACTIVE_PRODUCT: {
            return { ...state, productData: { products: action.payload.productWithImage, count: action.payload.totalHits } }
        }
        
        case Type.PRODUCT_FIELD_CHANGE: {
            return { ...state, data: { ...state.data, [action.payload.prop]: action.payload.value } }
        }

        case Type.SAVE_PRODUCT: {
            return { ...state, data: { ...state.data, [action.payload.prop]: action.payload.value } }
        }

        case Type.ADD_PRODUCT_ITEM: {
            return { ...state, data: { ...state.data, itemIds: [...state.data.itemIds, action.payload.value] } }
        }

        case Type.REMOVE_PRODUCT_ITEM: {
            return { ...state, data: { ...state.data, itemIds: [...state.data.itemIds.filter((item, index) => index !== action.payload.index)] } }
        }

        case Type.INITIALIZE_PRODUCT: {
            return undefined;
        } 
        
        case Type.FILTER_PRODUCT_SET_SIZE: {
            return {
                ...state, filter: {
                    ...state.filter,
                    pagination: {
                        ...state.filter.pagination,
                        size: action.payload
                    }
                }
            }
        }
        
        case Type.FILTER_PRODUCT_SET_PAGE: {
            return {
                ...state, filter: {
                    ...state.filter,
                    pagination: {
                        ...state.filter.pagination,
                        from: action.payload
                    }
                }
            }
        }

        case Type.ADD_PRODUCT_IMAGE: {
            return { ...state, data: { ...state.data, base64Images: [...state.data.base64Images, action.payload.value] } }
        }

        case Type.REMOVE_PRODUCT_IMAGE: {
            return { ...state, data: { ...state.data, base64Images: [...state.data.base64Images.filter((item, index) => index !== action.payload.index)] } }
        }

        case Type.ADD_PRODUCT_HASHTAG: {
            return { ...state, data: { ...state.data, hashtags: [...state.data.hashtags, action.payload.value] } }
        }
        case Type.REMOVE_PRODUCT_HASHTAG: {
            return { ...state, data: { ...state.data, hashtags: [...state.data.hashtags.filter((item, index) => index !== action.payload.index)] } }
        } 

        case Type.ACTIVATE_PRODUCT: {
            return {
                ...state, productData: {
                    ...state.productData, products: [...state.productData.products.map((item, index) => {
                        if (item.id === action.payload) {
                            return { ...item, isActive: !item.isActive}
                        }
                    })]
                }
            }
        }

        case Type.SELECT_PRODUCT : {
            return {
                ...state, data: {  
                    edit: true,                                                                 
                    id: action.payload.id,
                    active: action.payload.isActive,
                    addressTag: action.payload.addressIndex,
                    base64CoverImage: action.payload.coverImage.imageId,
                    base64Images: action.payload.images.map((item) => {
                            return item.imageId
                    }),
                    categoryCode: action.payload.category.id,
                    description: action.payload.description,
                    hashtags: action.payload.hashTags.map((hash)=> {return hash.hashtag}),
                    itemIds: action.payload.itemIds,
                    merchantId: null,
                    name: action.payload.name,
                    negotiable: action.payload.isNegotiable,
                    price: action.payload.price
                }
            }
        } case Type.INITIALIZE_PRODUCT: {
            return  state;
        }

        default: return state
    }
}

const setMatchParam = (existingParams, key, value) => {
    const existingItem = [...existingParams.filter((item) => item.key !== key)];
    const newItem = [...existingItem.map((item) => {
        if (item.key === key) {
            return { ...item, value: value }
        } else {
            return ({ key: key, value: value });
        }
    })];


    return [...newItem, ...existingItem];
}