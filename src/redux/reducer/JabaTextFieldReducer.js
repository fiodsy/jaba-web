import * as Type from '../action/types';


const INITIAL_STATE = {
    fieldData: {
        organizationName: '',
        base64Logo: '',
        textContents:[]
    },
    errorMessage: ''
};

export default (state = INITIAL_STATE, action) => {

    switch (action.type) {

        case Type.FIELD_CHANGE: {
            return { ...state, fieldData: { ...state.fieldData,[action.payload.prop]: action.payload.value } }
        }
        default: return state
    }

    return state;

}