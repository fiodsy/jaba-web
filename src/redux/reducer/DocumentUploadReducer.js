
import * as Type from '../action/types';


const INITIAL_STATE = {
    documentResponse: [],
    currentIndex: 0,
    errorMessage: '',
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case Type.UPDATE_DOCUMENT: {
            return { ...state, documentResponse: [...state.documentResponse, action.payload.value] }
        }
        case Type.REMOVE_DOCUMENT: {
            // return { ...state, documentResponse: [...state.documentResponse, action.payload.value] }

            return { ...state, documentResponse: [...state.documentResponse.filter((item, index) => index !== action.payload.index)] }
        }
        case Type.UPDATE_DOCUMENT_FIELD_BY_INDEX: {

            return {
                ...state, documentResponse: [...state.documentResponse.map((item, index) => {
                    if (index === action.payload.index) {

                        return { ...item, [action.payload.prop]: action.payload.value }
                    }

                    return item;
                })]
            }

        }
    }
    return state;
}