import * as Type from './types';
import * as ApiService from '../../api/ApiService';

export const fieldChangeHandler = (prop, value) => {
    return {
        type: Type.FIELD_CHANGE,
        payload: { prop, value }
    }
}


export const fieldDisableHandler = (prop, value) => {
    return {
        type: Type.CHANGE_DISABLE_STATE,
        payload: { prop, value }
    }
}

export const updateAddress = (prop, value) => {
    return {
        type: Type.UPDATE_ADDRESS,
        payload: { prop, value }
    }
}

export const listProvinces = (token) => {
    return async (dispatch) => {

        if (!token) {
            dispatch({ type: Type.ADDRESS_FIELD_ERROR, payload: 'Invalid Token' })
            return
        }

        ApiService.listAllProvinces(token).then(({ data }) => {
            if (data.data) {
                dispatch({ type: Type.GET_PROVINCE_LIST, payload: data.data })
            } else {
                dispatch({ type: Type.ADDRESS_FIELD_ERROR, payload: 'Can\'t load provinces' })
            }
        });
    }
}

export const listMunicipals = (token, code) => {
    return async (dispatch) => {

        if (!token) {
            dispatch({ type: Type.ADDRESS_FIELD_ERROR, payload: 'Invalid Token' })
            return
        }
        ApiService.listAllCityMunicipality(token, code).then(({ data }) => {
            if (data.data) {
                dispatch({ type: Type.GET_MUNICIPAL_LIST, payload: data.data })
            } else {
                dispatch({ type: Type.ADDRESS_FIELD_ERROR, payload: 'Can\'t load municipals' })
            }
        });
    }
}

export const listBarangays = (token, code) => {
    return async (dispatch) => {

        if (!token) {
            dispatch({ type: Type.ADDRESS_FIELD_ERROR, payload: 'Invalid Token' })
            return
        }

        ApiService.listAllBarangays(token, code).then(({ data }) => {
            if (data.data) {
                dispatch({ type: Type.GET_BARANGAY_LIST, payload: data.data })
            } else {
                dispatch({ type: Type.ADDRESS_FIELD_ERROR, payload: 'Can\'t load barangays' })
            }
        });
    }
}

export const updateContactFieldProps = (value) => {
    return {
        type: Type.UPDATE_FIELD_PROPS,
        payload: { value }
    }
}

export const incrementIndex = () => {
    return {
        type: Type.INCREMENT_INDEX
    }
}


export const updateContactInformation = (prop, value) => {
    return {
        type: Type.UPDATE_CONTACT_INFORMATION,
        payload: { value, prop }
    }
}

export const updateContactInformationByIndex = (prop, value, index) => {
    return {
        type: Type.UPDATE_CONTACT_INFORMATION_BY_INDEX,
        payload: { value, prop, index }
    }
}

export const getContactInformationByIndex = (prop, index) => {
    return {
        type: Type.GET_CONTACT_INFORMATION_BY_INDEX,
        payload: { prop, index }
    }
}


export const updateDocumentByIndex = (prop, index) => {
    return {
        type: Type.UPDATE_DOCUMENT_BY_INDEX,
        payload: { prop, index }
    }
}

export const updateDocument = (value) => {
    return {
        type: Type.UPDATE_DOCUMENT,
        payload: { value }
    }
}

export const updateDocumentFieldByIndex = (prop, value, index) => {
    return {
        type: Type.UPDATE_DOCUMENT_FIELD_BY_INDEX,
        payload: { prop, value, index }
    }
}

export const removeDocument = (index) => {
    return {
        type: Type.REMOVE_DOCUMENT,
        payload: { index }
    }
}

export const fetchItemTypeLookUp = (token, lookup) => {
    return async (dispatch) => {

        if (!token) {
            dispatch({ type: Type.LOOKUP_FETCH_ERROR, payload: 'Invalid Token' })
            return
        }

        ApiService.fetchLookup(token, lookup).then(({ data }) => {
            if (data.data) {
                dispatch({ type: Type.FETCH_ITEM_TYPE, payload: data.data })
            } else {
                dispatch({ type: Type.LOOKUP_FETCH_ERROR, payload: 'Can\'t load Product Type' })
            }
        });
    }
}

export const fetchProductCategoryLookUp = (token, lookup) => {
    return async (dispatch) => {
        if (!token) {
            dispatch({ type: Type.LOOKUP_FETCH_ERROR, payload: 'Invalid Token' })
            return
        }

        ApiService.fetchLookup(token, lookup).then(({ data }) => {
            if (data.data) {
                dispatch({ type: Type.FETCH_PRODUCT_CATEGORY, payload: data.data })
            } else {
                dispatch({ type: Type.LOOKUP_FETCH_ERROR, payload: 'Can\'t load Product Type' })
            }
        });
    }
}

//Item
export const itemFieldChangeHandler = (prop, value) => {
    return {
        type: Type.ITEM_FIELD_CHANGE,
        payload: { prop, value }
    }
}

export const addHashTag = (value) => {
    return {
        type: Type.ADD_HASHTAG,
        payload: { value }
    }
}

export const removeHashTag = (index) => {
    return {
        type: Type.REMOVE_HASHTAG,
        payload: { index }
    }
}

export const fetchAllItem = (token, parameters) => {

    return async (dispatch) => {
        if (!token) {
            dispatch({ type: Type.FETCH_ITEM_ERROR, payload: 'Invalid Token' })
            return
        }

        ApiService.fetchAllItem(token, parameters).then(({ data }) => {
            if (data.data) {
                dispatch({ type: Type.FETCH_ALL_ACTIVE_ITEM, payload: data.data })
            } else {
                dispatch({ type: Type.FETCH_ITEM_ERROR, payload: 'Can\'t Fetch Item' })
            }
        });
    }
}

export const addItemParameterMatches = (key, value) => {
    return {
        type: Type.FILTER_ITEM_MATCHES,
        payload: { key, value }
    }
}

export const addItemParameterLike = (key, value) => {
    return {
        type: Type.FILTER_ITEM_MATCHES_LIKE,
        payload: { key, value }
    }
}

export const setItemSize = (value) => {
    return {
        type: Type.FILTER_ITEM_SET_SIZE,
        payload: value
    }
}

export const setItemPage = (value) => {
    return {
        type: Type.FILTER_ITEM_SET_PAGE,
        payload: value
    }
}

export const initializeItemFilter = () => {
    return {
        type: Type.INITIALIZE_ITEM_FILTER
    }
}

export const initializeItem = () => {
    return {
        type: Type.INITIALIZE_ITEM
    }
}


export const addItemImage = (value) => {
    return {
        type: Type.ADD_ITEM_IMAGE,
        payload: { value }
    }
}

export const removeItemImage = (index) => {
    return {
        type: Type.REMOVE_ITEM_IMAGE,
        payload: { index }
    }
}


//IMAGE UPLOAD
export const addImage = (value) => {
    return {
        type: Type.ADD_IMAGE,
        payload: { value }
    }
}


export const updateImageFieldByIndex = (prop) => {
    return {
        type: Type.UPDATE_IMAGE_BY_INDEX,
        payload: { prop }
    }
}

export const removeImage = (index) => {
    return {
        type: Type.REMOVE_IMAGE,
        payload: { index }
    }
}

export const getImageByIndex = (index) => {
    return {
        type: Type.GET_IMAGE_BY_INDEX,
        payload: { index }
    }
}



//PRODUCT 
export const initializeProduct = () => {
    return {
        type: Type.INITIALIZE_PRODUCT
    }
}


export const productFieldChangeHandler = (prop, value) => {
    return {
        type: Type.PRODUCT_FIELD_CHANGE,
        payload: { prop, value }
    }
}

export const addProductItem = (value) => {
    return {
        type: Type.ADD_PRODUCT_ITEM,
        payload: { value }
    }
}

export const removeProductItem = (index) => {
    return {
        type: Type.REMOVE_PRODUCT_ITEM,
        payload: { index }
    }
}

export const fetchAllProducts = (token, parameters) => {

    return async (dispatch) => {
        if (!token) {
            dispatch({ type: Type.FETCH_PRODUCT_ERROR, payload: 'Invalid Token' })
            return
        }

        ApiService.fetchAllProducts(token, parameters).then(({ data }) => {
            if (data.data) {
                dispatch({ type: Type.FETCH_ALL_ACTIVE_PRODUCT, payload: data.data })
            } else {
                dispatch({ type: Type.FETCH_PRODUCT_ERROR, payload: 'Can\'t Fetch Item' })
            }
        });
    }
}

export const setProductSize = (value) => {
    return {
        type: Type.FILTER_PRODUCT_SET_SIZE,
        payload: value
    }
}

export const setProductPage = (value) => {
    return {
        type: Type.FILTER_PRODUCT_SET_PAGE,
        payload: value
    }
}

export const addProductImage = (value) => {
    return {
        type: Type.ADD_PRODUCT_IMAGE,
        payload: { value }
    }
}

export const removeProductImage = (index) => {
    return {
        type: Type.REMOVE_PRODUCT_IMAGE,
        payload: { index }
    }
}


export const addProductHashTag = (value) => {
    return {
        type: Type.ADD_PRODUCT_HASHTAG,
        payload: { value }
    }
}

export const removeProductHashTag = (index) => {
    return {
        type: Type.REMOVE_PRODUCT_HASHTAG,
        payload: { index }
    }
}



export const activateProduct = (token, productId) => {
    return async (dispatch) => {
        if (!token) {
            dispatch({ type: Type.UPDATE_PRODUCT_ERROR, payload: 'Invalid Token' })
            return
        }

        ApiService.activateProduct(token, productId).then(({ data }) => {

            if (data.statusCode === 200) {
                dispatch({ type: Type.ACTIVATE_PRODUCT, payload: productId })
            } else {
                dispatch({ type: Type.UPDATE_PRODUCT_ERROR, payload: 'Can\'t Fetch Item' })
            }
        });
    }
}

export const selectProduct = (value) => {
    return {
        type: Type.SELECT_PRODUCT,
        payload: value
    }
}
