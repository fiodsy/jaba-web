export const INITIAL_STATE = 'initialState';

//TEXTFIELD
export const FIELD_CHANGE = 'field_change';

//ADDRESS
export const UPDATE_ADDRESS = 'updateAddress';
export const GET_PROVINCE_LIST = 'provinceList';
export const GET_MUNICIPAL_LIST = 'municipalList';
export const GET_BARANGAY_LIST = 'barangayList';
export const ADDRESS_FIELD_ERROR = 'addressError';
export const CHANGE_DISABLE_STATE = 'changeDisable';

//CONTACTNUMBER
export const GET_FIELD_PROPS = 'contactFieldProps';
export const UPDATE_FIELD_PROPS = 'contactFieldUpdate';
export const INCREMENT_INDEX = 'incrementIndex';
export const UPDATE_CONTACT_INFORMATION = 'updateContactInformation';
export const UPDATE_CONTACT_INFORMATION_BY_INDEX = 'updateContactInformationByIndex';
export const GET_CONTACT_INFORMATION_BY_INDEX = 'getContactInformationByIndex';


//DOCUMENTUPLOAD
export const UPDATE_DOCUMENT_BY_INDEX = 'updateDocumentByIndex';
export const UPDATE_DOCUMENT = 'updateDocument';
export const UPDATE_DOCUMENT_FIELD_BY_INDEX = 'updateDocumentFieldByIndex';
export const REMOVE_DOCUMENT = 'removeDocument';

//LOOKUP
export const LOOKUP_FETCH_ERROR = 'lookUpError';
export const FETCH_PRODUCT_CATEGORY = 'fetchProductCategory';
export const FETCH_ITEM_TYPE = 'fetchItemType';

//ITEM
export const INITIALIZE_ITEM_FILTER = 'initializeFilter';
export const ITEM_FIELD_CHANGE = 'fieldChangeItem';
export const ADD_HASHTAG = 'addHashTag';
export const REMOVE_HASHTAG = 'removeHashTag';
export const ADD_ITEM_IMAGE = 'addItemImage';
export const REMOVE_ITEM_IMAGE = 'removeItemImage';
export const FETCH_ALL_ACTIVE_ITEM = 'fetchAllItem';
export const FETCH_ITEM_ERROR = 'errorFetchItem';
export const FILTER_ITEM_MATCHES = 'filterItemMatches';
export const FILTER_ITEM_SET_SIZE = 'filterSetSize';
export const FILTER_ITEM_SET_PAGE = 'filterSetPage';
export const FILTER_ITEM_MATCHES_LIKE = 'filterMatchesLike';
export const INITIALIZE_ITEM = 'initializeItem';

//IMAGEUPLOAD
export const UPDATE_IMAGE_BY_INDEX = 'updateImageByIndex';
export const REMOVE_IMAGE = 'removeImage';
export const ADD_IMAGE = 'addImage';
export const GET_IMAGE_BY_INDEX = 'getImageByIndex';
//PRODUCT
export const FETCH_PRODUCT_ERROR = 'errorFetchProduct';
export const FETCH_ALL_ACTIVE_PRODUCT = 'fetchAllProduct';
export const FILTER_PRODUCT_SET_SIZE = 'filterSetSize';
export const FILTER_PRODUCT_SET_PAGE = 'filterSetPage';
export const SAVE_PRODUCT = 'saveProduct';
export const PRODUCT_FIELD_CHANGE = 'productFieldChange';
export const INITIALIZE_PRODUCT = 'initializeProduct';
export const ADD_PRODUCT_ITEM = 'addItem';
export const REMOVE_PRODUCT_ITEM = 'removeItem';
export const ADD_PRODUCT_IMAGE = 'addProductImage';
export const REMOVE_PRODUCT_IMAGE = 'removeProductImage';
export const ACTIVATE_PRODUCT = 'activateProduct';
export const UPDATE_PRODUCT_ERROR = 'errorUpdateProduct';
export const SELECT_PRODUCT = 'selectProduct';
export const ADD_PRODUCT_HASHTAG = "addProductHashtag";
export const REMOVE_PRODUCT_HASHTAG = "removeProductHashtag";
